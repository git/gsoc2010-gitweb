#!/usr/bin/perl

# gitweb - simple web interface to track changes in git repositories
#
# (C) 2005-2006, Kay Sievers <kay.sievers@vrfy.org>
# (C) 2005, Christian Gierke
#
# This program is licensed under the GPLv2

use strict;
use warnings;

use File::Spec;
# __DIR__ is taken from Dir::Self __DIR__ fragment
sub __DIR__ () {
	File::Spec->rel2abs(join '', (File::Spec->splitpath(__FILE__))[0, 1]);
}
use lib __DIR__ . '/lib';

use CGI qw(:standard :escapeHTML -nosticky);
use CGI::Carp qw(fatalsToBrowser set_message);
use Fcntl ':mode';
use File::Find qw();
use File::Basename qw(basename);

binmode STDOUT, ':utf8';

use Gitweb::Git;
use Gitweb::Config;
use Gitweb::Request;
use Gitweb::Escape;
use Gitweb::RepoConfig;
use Gitweb::View;
use Gitweb::Util;
use Gitweb::Format;
use Gitweb::Parse;

BEGIN {
	CGI->compile() if $ENV{'MOD_PERL'};
}

# Only configuration variables with build-time overridable
# defaults are listed below. The complete set of variables
# with their descriptions is listed in Gitweb::Config.
$version = "++GIT_VERSION++";

# $GIT is from Gitweb::Git
$GIT = "++GIT_BINDIR++/git";

$projectroot = "++GITWEB_PROJECTROOT++";
$project_maxdepth = "++GITWEB_PROJECT_MAXDEPTH++";

$home_link_str = "++GITWEB_HOME_LINK_STR++";
$site_name = "++GITWEB_SITENAME++"
                 || ($ENV{'SERVER_NAME'} || "Untitled") . " Git";
$site_header = "++GITWEB_SITE_HEADER++";
$home_text = "++GITWEB_HOMETEXT++";
$site_footer = "++GITWEB_SITE_FOOTER++";

@stylesheets = ("++GITWEB_CSS++");
$stylesheet = undef;
$logo = "++GITWEB_LOGO++";
$favicon = "++GITWEB_FAVICON++";
$javascript = "++GITWEB_JS++";

$projects_list = "++GITWEB_LIST++";

$export_ok = "++GITWEB_EXPORT_OK++";
$strict_export = "++GITWEB_STRICT_EXPORT++";

@git_base_url_list = grep { $_ ne '' } ("++GITWEB_BASE_URL++");

$GITWEB_CONFIG = $ENV{'GITWEB_CONFIG'} || "++GITWEB_CONFIG++";
$GITWEB_CONFIG_SYSTEM = $ENV{'GITWEB_CONFIG_SYSTEM'} || "++GITWEB_CONFIG_SYSTEM++";

# Get loadavg of system, to compare against $maxload.
# Currently it requires '/proc/loadavg' present to get loadavg;
# if it is not present it returns 0, which means no load checking.
sub get_loadavg {
	if( -e '/proc/loadavg' ){
		open my $fd, '<', '/proc/loadavg'
			or return 0;
		my @load = split(/\s+/, scalar <$fd>);
		close $fd;

		# The first three columns measure CPU and IO utilization of the last one,
		# five, and 10 minute periods.  The fourth column shows the number of
		# currently running processes and the total number of processes in the m/n
		# format.  The last column displays the last process ID used.
		return $load[0] || 0;
	}
	# additional checks for load average should go here for things that don't export
	# /proc/loadavg

	return 0;
}

sub check_loadavg {
	if (defined $maxload && get_loadavg() > $maxload) {
		die_error(503, "The load average on the server is too high");
	}
}

# ======================================================================
# input validation and dispatch

# we will also need to know the possible actions, for validation
our %actions = (
	"blame" => \&git_blame,
	"blame_incremental" => \&git_blame_incremental,
	"blame_data" => \&git_blame_data,
	"blobdiff" => \&git_blobdiff,
	"blobdiff_plain" => \&git_blobdiff_plain,
	"blob" => \&git_blob,
	"blob_plain" => \&git_blob_plain,
	"commitdiff" => \&git_commitdiff,
	"commitdiff_plain" => \&git_commitdiff_plain,
	"commit" => \&git_commit,
	"forks" => \&git_forks,
	"heads" => \&git_heads,
	"history" => \&git_history,
	"log" => \&git_log,
	"patch" => \&git_patch,
	"patches" => \&git_patches,
	"rss" => \&git_rss,
	"atom" => \&git_atom,
	"search" => \&git_search,
	"search_help" => \&git_search_help,
	"shortlog" => \&git_shortlog,
	"summary" => \&git_summary,
	"tag" => \&git_tag,
	"tags" => \&git_tags,
	"tree" => \&git_tree,
	"snapshot" => \&git_snapshot,
	"object" => \&git_object,
	# those below don't need $project
	"opml" => \&git_opml,
	"project_list" => \&git_project_list,
	"project_index" => \&git_project_index,
);

# we will also need to know the possible 'edits', for validation
our %edits = (
	#already existing subroutines
	"log" => \&git_log,
	"summary" => \&git_summary,
	#new subroutines
	"addrepo" => \&git_addrepo,
	"add" => \&git_add,
	"discard" => \&git_discard,
	"ignore" => \&git_ignore,
	"mv" => \&git_mv,
	"newrepo" => \&git_newrepo,
	"rm" => \&git_rm,
	"reset" => \&git_reset,
);

# now read PATH_INFO and update the parameter list for missing parameters
sub evaluate_path_info {
	return if defined $input_params{'project'};
	return if !$path_info;
	$path_info =~ s,^/+,,;
	return if !$path_info;

	# find which part of PATH_INFO is project
	my $project = $path_info;
	$project =~ s,/+$,,;
	while ($project && !check_head_link("$projectroot/$project")) {
		$project =~ s,/*[^/]*$,,;
	}
	return unless $project;
	$input_params{'project'} = $project;

	# do not change any parameters if an action is given using the query string
	return if $input_params{'action'};
	$path_info =~ s,^\Q$project\E/*,,;

	# next, check if we have an action
	my $action = $path_info;
	$action =~ s,/.*$,,;
	if (exists $actions{$action}) {
		$path_info =~ s,^$action/*,,;
		$input_params{'action'} = $action;
	}

	return if $input_params{'edit'};
	# next, check if we have an edit
	my $edit = $path_info;
	$edit =~ s,/.*$,,;
	if (exists $edits{$edit} && gitweb_check_feature('write')) {
		$path_info =~ s,^$edit/*,,;
		$input_params{'edit'} = $edit;
	}

	# list of actions that want hash_base instead of hash, but can have no
	# pathname (f) parameter
	my @wants_base = (
		'tree',
		'history',
	);

	# we want to catch
	# [$hash_parent_base[:$file_parent]..]$hash_parent[:$file_name]
	my ($parentrefname, $parentpathname, $refname, $pathname) =
		($path_info =~ /^(?:(.+?)(?::(.+))?\.\.)?(.+?)(?::(.+))?$/);

	# first, analyze the 'current' part
	if (defined $pathname) {
		# we got "branch:filename" or "branch:dir/"
		# we could use git_get_type(branch:pathname), but:
		# - it needs $git_dir
		# - it does a git() call
		# - the convention of terminating directories with a slash
		#   makes it superfluous
		# - embedding the action in the PATH_INFO would make it even
		#   more superfluous
		$pathname =~ s,^/+,,;
		if (!$pathname || substr($pathname, -1) eq "/") {
			$input_params{'action'} ||= "tree";
			$pathname =~ s,/$,,;
		} else {
			# the default action depends on whether we had parent info
			# or not
			if ($parentrefname) {
				$input_params{'action'} ||= "blobdiff_plain";
			} else {
				$input_params{'action'} ||= "blob_plain";
			}
		}
		$input_params{'hash_base'} ||= $refname;
		$input_params{'file_name'} ||= $pathname;
	} elsif (defined $refname) {
		# we got "branch". In this case we have to choose if we have to
		# set hash or hash_base.
		#
		# Most of the actions without a pathname only want hash to be
		# set, except for the ones specified in @wants_base that want
		# hash_base instead. It should also be noted that hand-crafted
		# links having 'history' as an action and no pathname or hash
		# set will fail, but that happens regardless of PATH_INFO.
		$input_params{'action'} ||= "shortlog";
		if (grep { $_ eq $input_params{'action'} } @wants_base) {
			$input_params{'hash_base'} ||= $refname;
		} else {
			$input_params{'hash'} ||= $refname;
		}
	}

	# next, handle the 'parent' part, if present
	if (defined $parentrefname) {
		# a missing pathspec defaults to the 'current' filename, allowing e.g.
		# someproject/blobdiff/oldrev..newrev:/filename
		if ($parentpathname) {
			$parentpathname =~ s,^/+,,;
			$parentpathname =~ s,/$,,;
			$input_params{'file_parent'} ||= $parentpathname;
		} else {
			$input_params{'file_parent'} ||= $input_params{'file_name'};
		}
		# we assume that hash_parent_base is wanted if a path was specified,
		# or if the action wants hash_base instead of hash
		if (defined $input_params{'file_parent'} ||
			grep { $_ eq $input_params{'action'} } @wants_base) {
			$input_params{'hash_parent_base'} ||= $parentrefname;
		} else {
			$input_params{'hash_parent'} ||= $parentrefname;
		}
	}

	# for the snapshot action, we allow URLs in the form
	# $project/snapshot/$hash.ext
	# where .ext determines the snapshot and gets removed from the
	# passed $refname to provide the $hash.
	#
	# To be able to tell that $refname includes the format extension, we
	# require the following two conditions to be satisfied:
	# - the hash input parameter MUST have been set from the $refname part
	#   of the URL (i.e. they must be equal)
	# - the snapshot format MUST NOT have been defined already (e.g. from
	#   CGI parameter sf)
	# It's also useless to try any matching unless $refname has a dot,
	# so we check for that too
	if (defined $input_params{'action'} &&
		$input_params{'action'} eq 'snapshot' &&
		defined $refname && index($refname, '.') != -1 &&
		$refname eq $input_params{'hash'} &&
		!defined $input_params{'snapshot_format'}) {
		# We loop over the known snapshot formats, checking for
		# extensions. Allowed extensions are both the defined suffix
		# (which includes the initial dot already) and the snapshot
		# format key itself, with a prepended dot
		while (my ($fmt, $opt) = each %known_snapshot_formats) {
			my $hash = $refname;
			unless ($hash =~ s/(\Q$opt->{'suffix'}\E|\Q.$fmt\E)$//) {
				next;
			}
			my $sfx = $1;
			# a valid suffix was found, so set the snapshot format
			# and reset the hash parameter
			$input_params{'snapshot_format'} = $fmt;
			$input_params{'hash'} = $hash;
			# we also set the format suffix to the one requested
			# in the URL: this way a request for e.g. .tgz returns
			# a .tgz instead of a .tar.gz
			$known_snapshot_formats{$fmt}{'suffix'} = $sfx;
			last;
		}
	}
}

sub evaluate_and_validate_params {
	$action = $input_params{'action'};
	if (defined $action) {
		if (!validate_action($action)) {
			die_error(400, "Invalid action parameter");
		}
	}

	$edit = $input_params{'edit'};
	if(defined $edit && gitweb_check_feature('write')) {
		if(!validate_edit($edit)) {
			die_error(400, "Invalid edit parameter");
		}
	}

	# parameters which are pathnames
	$project = $input_params{'project'};
	if (defined $project) {
		if (!validate_project($project)) {
			undef $project;
			die_error(404, "No such project");
		}
	}

	$file_name = $input_params{'file_name'};
	if (defined $file_name) {
		if (!validate_pathname($file_name)) {
			die_error(400, "Invalid file parameter");
		}
	}

	$file_parent = $input_params{'file_parent'};
	if (defined $file_parent) {
		if (!validate_pathname($file_parent)) {
			die_error(400, "Invalid file parent parameter");
		}
	}

	# parameters which are refnames
	$hash = $input_params{'hash'};
	if (defined $hash) {
		if (!validate_refname($hash)) {
			die_error(400, "Invalid hash parameter");
		}
	}

	$hash_parent = $input_params{'hash_parent'};
	if (defined $hash_parent) {
		if (!validate_refname($hash_parent)) {
			die_error(400, "Invalid hash parent parameter");
		}
	}

	$hash_base = $input_params{'hash_base'};
	if (defined $hash_base) {
		if (!validate_refname($hash_base)) {
			die_error(400, "Invalid hash base parameter");
		}
	}

	@extra_options = @{$input_params{'extra_options'}};
	# @extra_options is always defined, since it can only be (currently) set from
	# CGI, and $cgi->param() returns the empty array in array context if the param
	# is not set
	foreach my $opt (@extra_options) {
		if (not exists $allowed_options{$opt}) {
			die_error(400, "Invalid option parameter");
		}
		if (not grep(/^$action$/, @{$allowed_options{$opt}})) {
			die_error(400, "Invalid option parameter for this action");
		}
	}

	$hash_parent_base = $input_params{'hash_parent_base'};
	if (defined $hash_parent_base) {
		if (!validate_refname($hash_parent_base)) {
			die_error(400, "Invalid hash parent base parameter");
		}
	}

	# other parameters
	$page = $input_params{'page'};
	if (defined $page) {
		if ($page =~ m/[^0-9]/) {
			die_error(400, "Invalid page parameter");
		}
	}

	$searchtype = $input_params{'searchtype'};
	if (defined $searchtype) {
		if ($searchtype =~ m/[^a-z]/) {
			die_error(400, "Invalid searchtype parameter");
		}
	}

	$search_use_regexp = $input_params{'search_use_regexp'};

	$searchtext = $input_params{'searchtext'};
	if (defined $searchtext) {
		if (length($searchtext) < 2) {
			die_error(403, "At least two characters are required for search parameter");
		}
		$search_regexp = $search_use_regexp ? $searchtext : quotemeta $searchtext;
	}
}

sub evaluate_git_dir {
	$git_dir = "$projectroot/$project" if $project;
}

# custom error handler: 'die <message>' is Internal Server Error
sub handle_errors_html {
	my $msg = shift; # it is already HTML escaped

	# to avoid infinite loop where error occurs in die_error,
	# change handler to default handler, disabling handle_errors_html
	set_message("Error occured when inside die_error:\n$msg");

	# you cannot jump out of die_error when called as error handler;
	# the subroutine set via CGI::Carp::set_message is called _after_
	# HTTP headers are already written, so it cannot write them itself
	die_error(undef, undef, $msg, -error_handler => 1, -no_http_header => 1);
}
set_message(\&handle_errors_html);

# dispatch
sub dispatch {
	if (!defined $action) {
		if (defined $hash) {
			$action = git_get_type($hash);
		} elsif (defined $hash_base && defined $file_name) {
			$action = git_get_type("$hash_base:$file_name");
		} elsif (defined $project) {
			$action = 'summary';
		} else {
			$action = 'project_list';
		}
	}
	if (defined $edit) {
		$action = $edit;
		%actions = %edits;
	}
	if (!defined($actions{$action})) {
		die_error(400, "Unknown action or edit");
	}
	if ($action !~ m/^(?:opml|project_list|project_index)$/ &&
	    !$project) {
		die_error(400, "Project needed");
	}

	$actions{$action}->();
}

sub run_request {
	our $t0 = [Time::HiRes::gettimeofday()]
		if defined $t0;

	evaluate_uri();
	evaluate_gitweb_config();
	evaluate_git_version();
	check_loadavg();

	# $projectroot and $projects_list might be set in gitweb config file
	$projects_list ||= $projectroot;

	evaluate_query_params();
	evaluate_path_info();
	evaluate_and_validate_params();
	evaluate_git_dir();

	configure_gitweb_features();

	dispatch();
}

our $is_last_request = sub { 1 };
our ($pre_dispatch_hook, $post_dispatch_hook, $pre_listen_hook);
our $CGI = 'CGI';
sub configure_as_fcgi {
	require CGI::Fast;
	our $CGI = 'CGI::Fast';

	my $request_number = 0;
	# let each child service 100 requests
	our $is_last_request = sub { ++$request_number > 100 };
}
sub evaluate_argv {
	my $script_name = $ENV{'SCRIPT_NAME'} || $ENV{'SCRIPT_FILENAME'} || __FILE__;
	configure_as_fcgi()
		if $script_name =~ /\.fcgi$/;

	return unless (@ARGV);

	require Getopt::Long;
	Getopt::Long::GetOptions(
		'fastcgi|fcgi|f' => \&configure_as_fcgi,
		'nproc|n=i' => sub {
			my ($arg, $val) = @_;
			return unless eval { require FCGI::ProcManager; 1; };
			my $proc_manager = FCGI::ProcManager->new({
				n_processes => $val,
			});
			our $pre_listen_hook    = sub { $proc_manager->pm_manage()        };
			our $pre_dispatch_hook  = sub { $proc_manager->pm_pre_dispatch()  };
			our $post_dispatch_hook = sub { $proc_manager->pm_post_dispatch() };
		},
	);
}

sub run {
	evaluate_argv();

	$pre_listen_hook->()
		if $pre_listen_hook;

 REQUEST:
	while ($cgi = $CGI->new()) {
		$pre_dispatch_hook->()
			if $pre_dispatch_hook;

		run_request();

		$pre_dispatch_hook->()
			if $post_dispatch_hook;

		last REQUEST if ($is_last_request->());
	}

 DONE_GITWEB:
	1;
}

run();

if (defined caller) {
	# wrapped in a subroutine processing requests,
	# e.g. mod_perl with ModPerl::Registry, or PSGI with Plack::App::WrapCGI
	return;
} else {
	# pure CGI script, serving single request
	exit;
}

## ======================================================================
## validation, quoting/unquoting and escaping

sub validate_action {
	my $input = shift || return undef;
	return undef unless exists $actions{$input};
	return $input;
}

sub validate_edit {
	my $input = shift || return undef;
	return undef unless exists $edits{$input};
	return $input;
}

sub validate_project {
	my $input = shift || return undef;
	if (!validate_pathname($input) ||
		!(-d "$projectroot/$input") ||
		!check_export_ok("$projectroot/$input") ||
		($strict_export && !project_in_list($input))) {
		return undef;
	} else {
		return $input;
	}
}

sub validate_pathname {
	my $input = shift || return undef;

	# no '.' or '..' as elements of path, i.e. no '.' nor '..'
	# at the beginning, at the end, and between slashes.
	# also this catches doubled slashes
	if ($input =~ m!(^|/)(|\.|\.\.)(/|$)!) {
		return undef;
	}
	# no null characters
	if ($input =~ m!\0!) {
		return undef;
	}
	return $input;
}

sub validate_refname {
	my $input = shift || return undef;

	# textual hashes are O.K.
	if ($input =~ m/^[0-9a-fA-F]{40}$/) {
		return $input;
	}
	# it must be correct pathname
	$input = validate_pathname($input)
		or return undef;
	# restrictions on ref name according to git-check-ref-format
	if ($input =~ m!(/\.|\.\.|[\000-\040\177 ~^:?*\[]|/$)!) {
		return undef;
	}
	return $input;
}

## ......................................................................
## functions printing or outputting HTML: div

# Outputs the author name and date in long form
sub git_print_authorship {
	my $co = shift;
	my %opts = @_;
	my $tag = $opts{-tag} || 'div';
	my $author = $co->{'author_name'};

	my %ad = parse_date($co->{'author_epoch'}, $co->{'author_tz'});
	print "<$tag class=\"author_date\">" .
	      format_search_author($author, "author", esc_html($author)) .
	      " [$ad{'rfc2822'}";
	print_local_time(%ad) if ($opts{-localtime});
	print "]" . git_get_avatar($co->{'author_email'}, -pad_before => 1)
		  . "</$tag>\n";
}

# Outputs table rows containing the full author or committer information,
# in the format expected for 'commit' view (& similia).
# Parameters are a commit hash reference, followed by the list of people
# to output information for. If the list is empty it defalts to both
# author and committer.
sub git_print_authorship_rows {
	my $co = shift;
	# too bad we can't use @people = @_ || ('author', 'committer')
	my @people = @_;
	@people = ('author', 'committer') unless @people;
	foreach my $who (@people) {
		my %wd = parse_date($co->{"${who}_epoch"}, $co->{"${who}_tz"});
		print "<tr><td>$who</td><td>" .
		      format_search_author($co->{"${who}_name"}, $who,
			       esc_html($co->{"${who}_name"})) . " " .
		      format_search_author($co->{"${who}_email"}, $who,
			       esc_html("<" . $co->{"${who}_email"} . ">")) .
		      "</td><td rowspan=\"2\">" .
		      git_get_avatar($co->{"${who}_email"}, -size => 'double') .
		      "</td></tr>\n" .
		      "<tr>" .
		      "<td></td><td> $wd{'rfc2822'}";
		print_local_time(%wd);
		print "</td>" .
		      "</tr>\n";
	}
}

sub git_print_log {
	my $log = shift;
	my %opts = @_;

	if ($opts{'-remove_title'}) {
		# remove title, i.e. first line of log
		shift @$log;
	}
	# remove leading empty lines
	while (defined $log->[0] && $log->[0] eq "") {
		shift @$log;
	}

	# print log
	my $signoff = 0;
	my $empty = 0;
	foreach my $line (@$log) {
		if ($line =~ m/^ *(signed[ \-]off[ \-]by[ :]|acked[ \-]by[ :]|cc[ :])/i) {
			$signoff = 1;
			$empty = 0;
			if (! $opts{'-remove_signoff'}) {
				print "<span class=\"signoff\">" . esc_html($line) . "</span><br/>\n";
				next;
			} else {
				# remove signoff lines
				next;
			}
		} else {
			$signoff = 0;
		}

		# print only one empty line
		# do not print empty line after signoff
		if ($line eq "") {
			next if ($empty || $signoff);
			$empty = 1;
		} else {
			$empty = 0;
		}

		print format_log_line_html($line) . "<br/>\n";
	}

	if ($opts{'-final_empty_line'}) {
		# end with single empty line
		print "<br/>\n" unless $empty;
	}
}

## ......................................................................
## functions printing large fragments of HTML

sub git_difftree_body {
	my ($difftree, $hash, @parents) = @_;
	my ($parent) = $parents[0];
	my $have_blame = gitweb_check_feature('blame');
	print "<div class=\"list_head\">\n";
	if ($#{$difftree} > 10) {
		print(($#{$difftree} + 1) . " files changed:\n");
	}
	print "</div>\n";

	print "<table class=\"" .
	      (@parents > 1 ? "combined " : "") .
	      "diff_tree\">\n";

	# header only for combined diff in 'commitdiff' view
	my $has_header = @$difftree && @parents > 1 && $action eq 'commitdiff';
	if ($has_header) {
		# table header
		print "<thead><tr>\n" .
		       "<th></th><th></th>\n"; # filename, patchN link
		for (my $i = 0; $i < @parents; $i++) {
			my $par = $parents[$i];
			print "<th>" .
			      $cgi->a({-href => href(action=>"commitdiff",
			                             hash=>$hash, hash_parent=>$par),
			               -title => 'commitdiff to parent number ' .
			                          ($i+1) . ': ' . substr($par,0,7)},
			              $i+1) .
			      "&nbsp;</th>\n";
		}
		print "</tr></thead>\n<tbody>\n";
	}

	my $alternate = 1;
	my $patchno = 0;
	foreach my $line (@{$difftree}) {
		my $diff = parsed_difftree_line($line);

		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;

		if (exists $diff->{'nparents'}) { # combined diff

			fill_from_file_info($diff, @parents)
				unless exists $diff->{'from_file'};

			if (!is_deleted($diff)) {
				# file exists in the result (child) commit
				print "<td>" .
				      $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash),
				              -class => "list"}, esc_path($diff->{'to_file'})) .
				      "</td>\n";
			} else {
				print "<td>" .
				      esc_path($diff->{'to_file'}) .
				      "</td>\n";
			}

			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print "<td class=\"link\">" .
				      $cgi->a({-href => "#patch$patchno"}, "patch") .
				      " | " .
				      "</td>\n";
			}

			my $has_history = 0;
			my $not_deleted = 0;
			for (my $i = 0; $i < $diff->{'nparents'}; $i++) {
				my $hash_parent = $parents[$i];
				my $from_hash = $diff->{'from_id'}[$i];
				my $from_path = $diff->{'from_file'}[$i];
				my $status = $diff->{'status'}[$i];

				$has_history ||= ($status ne 'A');
				$not_deleted ||= ($status ne 'D');

				if ($status eq 'A') {
					print "<td  class=\"link\" align=\"right\"> | </td>\n";
				} elsif ($status eq 'D') {
					print "<td class=\"link\">" .
					      $cgi->a({-href => href(action=>"blob",
					                             hash_base=>$hash,
					                             hash=>$from_hash,
					                             file_name=>$from_path)},
					              "blob" . ($i+1)) .
					      " | </td>\n";
				} else {
					if ($diff->{'to_id'} eq $from_hash) {
						print "<td class=\"link nochange\">";
					} else {
						print "<td class=\"link\">";
					}
					print $cgi->a({-href => href(action=>"blobdiff",
					                             hash=>$diff->{'to_id'},
					                             hash_parent=>$from_hash,
					                             hash_base=>$hash,
					                             hash_parent_base=>$hash_parent,
					                             file_name=>$diff->{'to_file'},
					                             file_parent=>$from_path)},
					              "diff" . ($i+1)) .
					      " | </td>\n";
				}
			}

			print "<td class=\"link\">";
			if ($not_deleted) {
				print $cgi->a({-href => href(action=>"blob",
				                             hash=>$diff->{'to_id'},
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash)},
				              "blob");
				print " | " if ($has_history);
			}
			if ($has_history) {
				print $cgi->a({-href => href(action=>"history",
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash)},
				              "history");
			}
			print "</td>\n";

			print "</tr>\n";
			next; # instead of 'else' clause, to avoid extra indent
		}
		# else ordinary diff

		my ($to_mode_oct, $to_mode_str, $to_file_type);
		my ($from_mode_oct, $from_mode_str, $from_file_type);
		if ($diff->{'to_mode'} ne ('0' x 6)) {
			$to_mode_oct = oct $diff->{'to_mode'};
			if (S_ISREG($to_mode_oct)) { # only for regular file
				$to_mode_str = sprintf("%04o", $to_mode_oct & 0777); # permission bits
			}
			$to_file_type = file_type($diff->{'to_mode'});
		}
		if ($diff->{'from_mode'} ne ('0' x 6)) {
			$from_mode_oct = oct $diff->{'from_mode'};
			if (S_ISREG($to_mode_oct)) { # only for regular file
				$from_mode_str = sprintf("%04o", $from_mode_oct & 0777); # permission bits
			}
			$from_file_type = file_type($diff->{'from_mode'});
		}

		if ($diff->{'status'} eq "A") { # created
			my $mode_chng = "<span class=\"file_status new\">[new $to_file_type";
			$mode_chng   .= " with mode: $to_mode_str" if $to_mode_str;
			$mode_chng   .= "]</span>";
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'}),
			              -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chng</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print $cgi->a({-href => "#patch$patchno"}, "patch");
				print " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'})},
			              "blob");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "D") { # deleted
			my $mode_chng = "<span class=\"file_status deleted\">[deleted $from_file_type]</span>";
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'from_id'},
			                             hash_base=>$parent, file_name=>$diff->{'file'}),
			               -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chng</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print $cgi->a({-href => "#patch$patchno"}, "patch");
				print " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'from_id'},
			                             hash_base=>$parent, file_name=>$diff->{'file'})},
			              "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>"blame", hash_base=>$parent,
				                             file_name=>$diff->{'file'})},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$parent,
			                             file_name=>$diff->{'file'})},
			              "history");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "M" || $diff->{'status'} eq "T") { # modified, or type changed
			my $mode_chnge = "";
			if ($diff->{'from_mode'} != $diff->{'to_mode'}) {
				$mode_chnge = "<span class=\"file_status mode_chnge\">[changed";
				if ($from_file_type ne $to_file_type) {
					$mode_chnge .= " from $from_file_type to $to_file_type";
				}
				if (($from_mode_oct & 0777) != ($to_mode_oct & 0777)) {
					if ($from_mode_str && $to_mode_str) {
						$mode_chnge .= " mode: $from_mode_str->$to_mode_str";
					} elsif ($to_mode_str) {
						$mode_chnge .= " mode: $to_mode_str";
					}
				}
				$mode_chnge .= "]</span>\n";
			}
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'}),
			              -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chnge</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print $cgi->a({-href => "#patch$patchno"}, "patch") .
				      " | ";
			} elsif ($diff->{'to_id'} ne $diff->{'from_id'}) {
				# "commit" view and modified file (not onlu mode changed)
				print $cgi->a({-href => href(action=>"blobdiff",
				                             hash=>$diff->{'to_id'}, hash_parent=>$diff->{'from_id'},
				                             hash_base=>$hash, hash_parent_base=>$parent,
				                             file_name=>$diff->{'file'})},
				              "diff") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'})},
			               "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>"blame", hash_base=>$hash,
				                             file_name=>$diff->{'file'})},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$hash,
			                             file_name=>$diff->{'file'})},
			              "history");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "R" || $diff->{'status'} eq "C") { # renamed or copied
			my %status_name = ('R' => 'moved', 'C' => 'copied');
			my $nstatus = $status_name{$diff->{'status'}};
			my $mode_chng = "";
			if ($diff->{'from_mode'} != $diff->{'to_mode'}) {
				# mode also for directories, so we cannot use $to_mode_str
				$mode_chng = sprintf(", mode: %04o", $to_mode_oct & 0777);
			}
			print "<td>" .
			      $cgi->a({-href => href(action=>"blob", hash_base=>$hash,
			                             hash=>$diff->{'to_id'}, file_name=>$diff->{'to_file'}),
			              -class => "list"}, esc_path($diff->{'to_file'})) . "</td>\n" .
			      "<td><span class=\"file_status $nstatus\">[$nstatus from " .
			      $cgi->a({-href => href(action=>"blob", hash_base=>$parent,
			                             hash=>$diff->{'from_id'}, file_name=>$diff->{'from_file'}),
			              -class => "list"}, esc_path($diff->{'from_file'})) .
			      " with " . (int $diff->{'similarity'}) . "% similarity$mode_chng]</span></td>\n" .
			      "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print $cgi->a({-href => "#patch$patchno"}, "patch") .
				      " | ";
			} elsif ($diff->{'to_id'} ne $diff->{'from_id'}) {
				# "commit" view and modified file (not only pure rename or copy)
				print $cgi->a({-href => href(action=>"blobdiff",
				                             hash=>$diff->{'to_id'}, hash_parent=>$diff->{'from_id'},
				                             hash_base=>$hash, hash_parent_base=>$parent,
				                             file_name=>$diff->{'to_file'}, file_parent=>$diff->{'from_file'})},
				              "diff") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$parent, file_name=>$diff->{'to_file'})},
			              "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>"blame", hash_base=>$hash,
				                             file_name=>$diff->{'to_file'})},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$hash,
			                            file_name=>$diff->{'to_file'})},
			              "history");
			print "</td>\n";

		} # we should not encounter Unmerged (U) or Unknown (X) status
		print "</tr>\n";
	}
	print "</tbody>" if $has_header;
	print "</table>\n";
}

sub git_patchset_body {
	my ($fd, $difftree, $hash, @hash_parents) = @_;
	my ($hash_parent) = $hash_parents[0];

	my $is_combined = (@hash_parents > 1);
	my $patch_idx = 0;
	my $patch_number = 0;
	my $patch_line;
	my $diffinfo;
	my $to_name;
	my (%from, %to);

	print "<div class=\"patchset\">\n";

	# skip to first patch
	while ($patch_line = <$fd>) {
		chomp $patch_line;

		last if ($patch_line =~ m/^diff /);
	}

 PATCH:
	while ($patch_line) {

		# parse "git diff" header line
		if ($patch_line =~ m/^diff --git (\"(?:[^\\\"]*(?:\\.[^\\\"]*)*)\"|[^ "]*) (.*)$/) {
			# $1 is from_name, which we do not use
			$to_name = unquote($2);
			$to_name =~ s!^b/!!;
		} elsif ($patch_line =~ m/^diff --(cc|combined) ("?.*"?)$/) {
			# $1 is 'cc' or 'combined', which we do not use
			$to_name = unquote($2);
		} else {
			$to_name = undef;
		}

		# check if current patch belong to current raw line
		# and parse raw git-diff line if needed
		if (is_patch_split($diffinfo, { 'to_file' => $to_name })) {
			# this is continuation of a split patch
			print "<div class=\"patch cont\">\n";
		} else {
			# advance raw git-diff output if needed
			$patch_idx++ if defined $diffinfo;

			# read and prepare patch information
			$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);

			# compact combined diff output can have some patches skipped
			# find which patch (using pathname of result) we are at now;
			if ($is_combined) {
				while ($to_name ne $diffinfo->{'to_file'}) {
					print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n" .
					      format_diff_cc_simplified($diffinfo, @hash_parents) .
					      "</div>\n";  # class="patch"

					$patch_idx++;
					$patch_number++;

					last if $patch_idx > $#$difftree;
					$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);
				}
			}

			# modifies %from, %to hashes
			parse_from_to_diffinfo($diffinfo, \%from, \%to, @hash_parents);

			# this is first patch for raw difftree line with $patch_idx index
			# we index @$difftree array from 0, but number patches from 1
			print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n";
		}

		# git diff header
		#assert($patch_line =~ m/^diff /) if DEBUG;
		#assert($patch_line !~ m!$/$!) if DEBUG; # is chomp-ed
		$patch_number++;
		# print "git diff" header
		print format_git_diff_header_line($patch_line, $diffinfo,
		                                  \%from, \%to);

		# print extended diff header
		print "<div class=\"diff extended_header\">\n";
	EXTENDED_HEADER:
		while ($patch_line = <$fd>) {
			chomp $patch_line;

			last EXTENDED_HEADER if ($patch_line =~ m/^--- |^diff /);

			print format_extended_diff_header_line($patch_line, $diffinfo,
			                                       \%from, \%to);
		}
		print "</div>\n"; # class="diff extended_header"

		# from-file/to-file diff header
		if (! $patch_line) {
			print "</div>\n"; # class="patch"
			last PATCH;
		}
		next PATCH if ($patch_line =~ m/^diff /);
		#assert($patch_line =~ m/^---/) if DEBUG;

		my $last_patch_line = $patch_line;
		$patch_line = <$fd>;
		chomp $patch_line;
		#assert($patch_line =~ m/^\+\+\+/) if DEBUG;

		print format_diff_from_to_header($last_patch_line, $patch_line,
		                                 $diffinfo, \%from, \%to,
		                                 @hash_parents);

		# the patch itself
	LINE:
		while ($patch_line = <$fd>) {
			chomp $patch_line;

			next PATCH if ($patch_line =~ m/^diff /);

			print format_diff_line($patch_line, \%from, \%to);
		}

	} continue {
		print "</div>\n"; # class="patch"
	}

	# for compact combined (--cc) format, with chunk and patch simpliciaction
	# patchset might be empty, but there might be unprocessed raw lines
	for (++$patch_idx if $patch_number > 0;
	     $patch_idx < @$difftree;
	     ++$patch_idx) {
		# read and prepare patch information
		$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);

		# generate anchor for "patch" links in difftree / whatchanged part
		print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n" .
		      format_diff_cc_simplified($diffinfo, @hash_parents) .
		      "</div>\n";  # class="patch"

		$patch_number++;
	}

	if ($patch_number == 0) {
		if (@hash_parents > 1) {
			print "<div class=\"diff nodifferences\">Trivial merge</div>\n";
		} else {
			print "<div class=\"diff nodifferences\">No differences found</div>\n";
		}
	}

	print "</div>\n"; # class="patchset"
}

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

# fills project list info (age, description, owner, forks) for each
# project in the list, removing invalid projects from returned list
# NOTE: modifies $projlist, but does not remove entries from it
sub fill_project_list_info {
	my ($projlist, $check_forks) = @_;
	my @projects;

	my $show_ctags = gitweb_check_feature('ctags');
 PROJECT:
	foreach my $pr (@$projlist) {
		my (@activity) = git_get_last_activity($pr->{'path'});
		unless (@activity) {
			next PROJECT;
		}
		($pr->{'age'}, $pr->{'age_string'}) = @activity;
		if (!defined $pr->{'descr'}) {
			my $descr = git_get_project_description($pr->{'path'}) || "";
			$descr = to_utf8($descr);
			$pr->{'descr_long'} = $descr;
			$pr->{'descr'} = chop_str($descr, $projects_list_description_width, 5);
		}
		if (!defined $pr->{'owner'}) {
			$pr->{'owner'} = git_get_project_owner("$pr->{'path'}") || "";
		}
		if ($check_forks) {
			my $pname = $pr->{'path'};
			if (($pname =~ s/\.git$//) &&
			    ($pname !~ /\/$/) &&
			    (-d "$projectroot/$pname")) {
				$pr->{'forks'} = "-d $projectroot/$pname";
			} else {
				$pr->{'forks'} = 0;
			}
		}
		$show_ctags and $pr->{'ctags'} = git_get_project_ctags($pr->{'path'});
		push @projects, $pr;
	}

	return @projects;
}

sub git_project_list_body {
	# actually uses global variable $project
	my ($projlist, $order, $from, $to, $extra, $no_header) = @_;

	my $check_forks = gitweb_check_feature('forks');
	my @projects = fill_project_list_info($projlist, $check_forks);

	$order ||= $default_projects_order;
	$from = 0 unless defined $from;
	$to = $#projects if (!defined $to || $#projects < $to);

	my %order_info = (
		project => { key => 'path', type => 'str' },
		descr => { key => 'descr_long', type => 'str' },
		owner => { key => 'owner', type => 'str' },
		age => { key => 'age', type => 'num' }
	);
	my $oi = $order_info{$order};
	if ($oi->{'type'} eq 'str') {
		@projects = sort {$a->{$oi->{'key'}} cmp $b->{$oi->{'key'}}} @projects;
	} else {
		@projects = sort {$a->{$oi->{'key'}} <=> $b->{$oi->{'key'}}} @projects;
	}

	my $show_ctags = gitweb_check_feature('ctags');
	if ($show_ctags) {
		my %ctags;
		foreach my $p (@projects) {
			foreach my $ct (keys %{$p->{'ctags'}}) {
				$ctags{$ct} += $p->{'ctags'}->{$ct};
			}
		}
		my $cloud = git_populate_project_tagcloud(\%ctags);
		print git_show_project_tagcloud($cloud, 64);
	}

	print "<table class=\"project_list\">\n";
	unless ($no_header) {
		print "<tr>\n";
		if ($check_forks) {
			print "<th></th>\n";
		}
		print_sort_th('project', $order, 'Project');
		print_sort_th('descr', $order, 'Description');
		print_sort_th('owner', $order, 'Owner');
		print_sort_th('age', $order, 'Last Change');
		print "<th></th>\n" . # for links
		      "</tr>\n";
	}
	my $alternate = 1;
	my $tagfilter = $cgi->param('by_tag');
	for (my $i = $from; $i <= $to; $i++) {
		my $pr = $projects[$i];

		next if $tagfilter and $show_ctags and not grep { lc $_ eq lc $tagfilter } keys %{$pr->{'ctags'}};
		next if $searchtext and not $pr->{'path'} =~ /$searchtext/
			and not $pr->{'descr_long'} =~ /$searchtext/;
		# Weed out forks or non-matching entries of search
		if ($check_forks) {
			my $forkbase = $project; $forkbase ||= ''; $forkbase =~ s#\.git$#/#;
			$forkbase="^$forkbase" if $forkbase;
			next if not $searchtext and not $tagfilter and $show_ctags
				and $pr->{'path'} =~ m#$forkbase.*/.*#; # regexp-safe
		}

		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		if ($check_forks) {
			print "<td>";
			if ($pr->{'forks'}) {
				print "<!-- $pr->{'forks'} -->\n";
				print $cgi->a({-href => href(project=>$pr->{'path'}, action=>"forks")}, "+");
			}
			print "</td>\n";
		}
		print "<td>" . $cgi->a({-href => href(project=>$pr->{'path'}, action=>"summary"),
		                        -class => "list"}, esc_html($pr->{'path'})) . "</td>\n" .
		      "<td>" . $cgi->a({-href => href(project=>$pr->{'path'}, action=>"summary"),
		                        -class => "list", -title => $pr->{'descr_long'}},
		                        esc_html($pr->{'descr'})) . "</td>\n" .
		      "<td><i>" . chop_and_escape_str($pr->{'owner'}, 15) . "</i></td>\n";
		print "<td class=\"". age_class($pr->{'age'}) . "\">" .
		      (defined $pr->{'age_string'} ? $pr->{'age_string'} : "No commits") . "</td>\n" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(project=>$pr->{'path'}, action=>"summary")}, "summary")   . " | " .
		      $cgi->a({-href => href(project=>$pr->{'path'}, action=>"shortlog")}, "shortlog") . " | " .
		      $cgi->a({-href => href(project=>$pr->{'path'}, action=>"log")}, "log") . " | " .
		      $cgi->a({-href => href(project=>$pr->{'path'}, action=>"tree")}, "tree") .
		      ($pr->{'forks'} ? " | " . $cgi->a({-href => href(project=>$pr->{'path'}, action=>"forks")}, "forks") : '') .
		      "</td>\n" .
		      "</tr>\n";
	}
	if (defined $extra) {
		print "<tr>\n";
		if ($check_forks) {
			print "<td></td>\n";
		}
		print "<td colspan=\"5\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

sub git_log_body {
	# uses global variable $project
	my ($commitlist, $from, $to, $refs, $extra) = @_;

	$from = 0 unless defined $from;
	$to = $#{$commitlist} if (!defined $to || $#{$commitlist} < $to);

	for (my $i = 0; $i <= $to; $i++) {
		my %co = %{$commitlist->[$i]};
		next if !%co;
		my $commit = $co{'id'};
		my $ref = format_ref_marker($refs, $commit);
		my %ad = parse_date($co{'author_epoch'});
		git_print_header_div('commit',
		               "<span class=\"age\">$co{'age_string'}</span>" .
		               esc_html($co{'title'}) . $ref,
		               $commit);
		print "<div class=\"title_text\">\n" .
		      "<div class=\"log_link\">\n" .
		      $cgi->a({-href => href(action=>"commit", hash=>$commit)}, "commit") .
		      " | " .
		      $cgi->a({-href => href(action=>"commitdiff", hash=>$commit)}, "commitdiff") .
		      " | " .
		      $cgi->a({-href => href(action=>"tree", hash=>$commit, hash_base=>$commit)}, "tree") .
		      "<br/>\n" .
		      "</div>\n";
		      git_print_authorship(\%co, -tag => 'span');
		      print "<br/>\n</div>\n";

		print "<div class=\"log_body\">\n";
		git_print_log($co{'comment'}, -final_empty_line=> 1);
		print "</div>\n";
	}
	if ($extra) {
		print "<div class=\"page_nav\">\n";
		print "$extra\n";
		print "</div>\n";
	}
}

sub git_shortlog_body {
	# uses global variable $project
	my ($commitlist, $from, $to, $refs, $extra) = @_;

	$from = 0 unless defined $from;
	$to = $#{$commitlist} if (!defined $to || $#{$commitlist} < $to);

	print "<table class=\"shortlog\">\n";
	my $alternate = 1;
	for (my $i = $from; $i <= $to; $i++) {
		my %co = %{$commitlist->[$i]};
		my $commit = $co{'id'};
		my $ref = format_ref_marker($refs, $commit);
		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		# git_summary() used print "<td><i>$co{'age_string'}</i></td>\n" .
		print "<td title=\"$co{'age_string_age'}\"><i>$co{'age_string_date'}</i></td>\n" .
		      format_author_html('td', \%co, 10) . "<td>";
		print format_subject_html($co{'title'}, $co{'title_short'},
		                          href(action=>"commit", hash=>$commit), $ref);
		print "</td>\n" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(action=>"commit", hash=>$commit)}, "commit") . " | " .
		      $cgi->a({-href => href(action=>"commitdiff", hash=>$commit)}, "commitdiff") . " | " .
		      $cgi->a({-href => href(action=>"tree", hash=>$commit, hash_base=>$commit)}, "tree");
		my $snapshot_links = format_snapshot_links($commit);
		if (defined $snapshot_links) {
			print " | " . $snapshot_links;
		}
		print "</td>\n" .
		      "</tr>\n";
	}
	if (defined $extra) {
		print "<tr>\n" .
		      "<td colspan=\"4\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

sub git_history_body {
	# Warning: assumes constant type (blob or tree) during history
	my ($commitlist, $from, $to, $refs, $extra,
	    $file_name, $file_hash, $ftype) = @_;

	$from = 0 unless defined $from;
	$to = $#{$commitlist} unless (defined $to && $to <= $#{$commitlist});

	print "<table class=\"history\">\n";
	my $alternate = 1;
	for (my $i = $from; $i <= $to; $i++) {
		my %co = %{$commitlist->[$i]};
		if (!%co) {
			next;
		}
		my $commit = $co{'id'};

		my $ref = format_ref_marker($refs, $commit);

		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		print "<td title=\"$co{'age_string_age'}\"><i>$co{'age_string_date'}</i></td>\n" .
	# shortlog:   format_author_html('td', \%co, 10)
		      format_author_html('td', \%co, 15, 3) . "<td>";
		# originally git_history used chop_str($co{'title'}, 50)
		print format_subject_html($co{'title'}, $co{'title_short'},
		                          href(action=>"commit", hash=>$commit), $ref);
		print "</td>\n" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(action=>$ftype, hash_base=>$commit, file_name=>$file_name)}, $ftype) . " | " .
		      $cgi->a({-href => href(action=>"commitdiff", hash=>$commit)}, "commitdiff");

		if ($ftype eq 'blob') {
			my $blob_current = $file_hash;
			my $blob_parent  = git_get_hash_by_path($commit, $file_name);
			if (defined $blob_current && defined $blob_parent &&
					$blob_current ne $blob_parent) {
				print " | " .
					$cgi->a({-href => href(action=>"blobdiff",
					                       hash=>$blob_current, hash_parent=>$blob_parent,
					                       hash_base=>$hash_base, hash_parent_base=>$commit,
					                       file_name=>$file_name)},
					        "diff to current");
			}
		}
		print "</td>\n" .
		      "</tr>\n";
	}
	if (defined $extra) {
		print "<tr>\n" .
		      "<td colspan=\"4\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

sub git_tags_body {
	# uses global variable $project
	my ($taglist, $from, $to, $extra) = @_;
	$from = 0 unless defined $from;
	$to = $#{$taglist} if (!defined $to || $#{$taglist} < $to);

	print "<table class=\"tags\">\n";
	my $alternate = 1;
	for (my $i = $from; $i <= $to; $i++) {
		my $entry = $taglist->[$i];
		my %tag = %$entry;
		my $comment = $tag{'subject'};
		my $comment_short;
		if (defined $comment) {
			$comment_short = chop_str($comment, 30, 5);
		}
		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		if (defined $tag{'age'}) {
			print "<td><i>$tag{'age'}</i></td>\n";
		} else {
			print "<td></td>\n";
		}
		print "<td>" .
		      $cgi->a({-href => href(action=>$tag{'reftype'}, hash=>$tag{'refid'}),
		               -class => "list name"}, esc_html($tag{'name'})) .
		      "</td>\n" .
		      "<td>";
		if (defined $comment) {
			print format_subject_html($comment, $comment_short,
			                          href(action=>"tag", hash=>$tag{'id'}));
		}
		print "</td>\n" .
		      "<td class=\"selflink\">";
		if ($tag{'type'} eq "tag") {
			print $cgi->a({-href => href(action=>"tag", hash=>$tag{'id'})}, "tag");
		} else {
			print "&nbsp;";
		}
		print "</td>\n" .
		      "<td class=\"link\">" . " | " .
		      $cgi->a({-href => href(action=>$tag{'reftype'}, hash=>$tag{'refid'})}, $tag{'reftype'});
		if ($tag{'reftype'} eq "commit") {
			print " | " . $cgi->a({-href => href(action=>"shortlog", hash=>$tag{'fullname'})}, "shortlog") .
			      " | " . $cgi->a({-href => href(action=>"log", hash=>$tag{'fullname'})}, "log");
		} elsif ($tag{'reftype'} eq "blob") {
			print " | " . $cgi->a({-href => href(action=>"blob_plain", hash=>$tag{'refid'})}, "raw");
		}
		print "</td>\n" .
		      "</tr>";
	}
	if (defined $extra) {
		print "<tr>\n" .
		      "<td colspan=\"5\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

sub git_heads_body {
	# uses global variable $project
	my ($headlist, $head, $from, $to, $extra) = @_;
	$from = 0 unless defined $from;
	$to = $#{$headlist} if (!defined $to || $#{$headlist} < $to);

	print "<table class=\"heads\">\n";
	my $alternate = 1;
	for (my $i = $from; $i <= $to; $i++) {
		my $entry = $headlist->[$i];
		my %ref = %$entry;
		my $curr = $ref{'id'} eq $head;
		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		print "<td><i>$ref{'age'}</i></td>\n" .
		      ($curr ? "<td class=\"current_head\">" : "<td>") .
		      $cgi->a({-href => href(action=>"shortlog", hash=>$ref{'fullname'}),
		               -class => "list name"},esc_html($ref{'name'})) .
		      "</td>\n" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(action=>"shortlog", hash=>$ref{'fullname'})}, "shortlog") . " | " .
		      $cgi->a({-href => href(action=>"log", hash=>$ref{'fullname'})}, "log") . " | " .
		      $cgi->a({-href => href(action=>"tree", hash=>$ref{'fullname'}, hash_base=>$ref{'name'})}, "tree") .
		      "</td>\n" .
		      "</tr>";
	}
	if (defined $extra) {
		print "<tr>\n" .
		      "<td colspan=\"3\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

sub git_search_grep_body {
	my ($commitlist, $from, $to, $extra) = @_;
	$from = 0 unless defined $from;
	$to = $#{$commitlist} if (!defined $to || $#{$commitlist} < $to);

	print "<table class=\"commit_search\">\n";
	my $alternate = 1;
	for (my $i = $from; $i <= $to; $i++) {
		my %co = %{$commitlist->[$i]};
		if (!%co) {
			next;
		}
		my $commit = $co{'id'};
		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;
		print "<td title=\"$co{'age_string_age'}\"><i>$co{'age_string_date'}</i></td>\n" .
		      format_author_html('td', \%co, 15, 5) .
		      "<td>" .
		      $cgi->a({-href => href(action=>"commit", hash=>$co{'id'}),
		               -class => "list subject"},
		              chop_and_escape_str($co{'title'}, 50) . "<br/>");
		my $comment = $co{'comment'};
		foreach my $line (@$comment) {
			if ($line =~ m/^(.*?)($search_regexp)(.*)$/i) {
				my ($lead, $match, $trail) = ($1, $2, $3);
				$match = chop_str($match, 70, 5, 'center');
				my $contextlen = int((80 - length($match))/2);
				$contextlen = 30 if ($contextlen > 30);
				$lead  = chop_str($lead,  $contextlen, 10, 'left');
				$trail = chop_str($trail, $contextlen, 10, 'right');

				$lead  = esc_html($lead);
				$match = esc_html($match);
				$trail = esc_html($trail);

				print "$lead<span class=\"match\">$match</span>$trail<br />";
			}
		}
		print "</td>\n" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(action=>"commit", hash=>$co{'id'})}, "commit") .
		      " | " .
		      $cgi->a({-href => href(action=>"commitdiff", hash=>$co{'id'})}, "commitdiff") .
		      " | " .
		      $cgi->a({-href => href(action=>"tree", hash=>$co{'tree'}, hash_base=>$co{'id'})}, "tree");
		print "</td>\n" .
		      "</tr>\n";
	}
	if (defined $extra) {
		print "<tr>\n" .
		      "<td colspan=\"3\">$extra</td>\n" .
		      "</tr>\n";
	}
	print "</table>\n";
}

## ======================================================================
## ======================================================================
## actions

sub git_project_list {
	my $order = $input_params{'order'};
	if (defined $order && $order !~ m/none|project|descr|owner|age/) {
		die_error(400, "Unknown order parameter");
	}

	my @list = git_get_projects_list();
	if (!@list) {
		die_error(404, "No projects found");
	}

	git_header_html();
	if (defined $home_text && -f $home_text) {
		print "<div class=\"index_include\">\n";
		insert_file($home_text);
		print "</div>\n";
	}
	print $cgi->startform(-method => "get") .
	      "<p class=\"projsearch\">Search:\n" .
	      $cgi->textfield(-name => "s", -value => $searchtext) . "\n" .
	      "</p>" .
	      $cgi->end_form() . "\n";
	git_project_list_body(\@list, $order);
	git_footer_html();
}

sub git_forks {
	my $order = $input_params{'order'};
	if (defined $order && $order !~ m/none|project|descr|owner|age/) {
		die_error(400, "Unknown order parameter");
	}

	my @list = git_get_projects_list($project);
	if (!@list) {
		die_error(404, "No forks found");
	}

	git_header_html();
	git_print_page_nav('','');
	git_print_header_div('summary', "$project forks");
	git_project_list_body(\@list, $order);
	git_footer_html();
}

sub git_project_index {
	my @projects = git_get_projects_list($project);

	print $cgi->header(
		-type => 'text/plain',
		-charset => 'utf-8',
		-content_disposition => 'inline; filename="index.aux"');

	foreach my $pr (@projects) {
		if (!exists $pr->{'owner'}) {
			$pr->{'owner'} = git_get_project_owner("$pr->{'path'}");
		}

		my ($path, $owner) = ($pr->{'path'}, $pr->{'owner'});
		# quote as in CGI::Util::encode, but keep the slash, and use '+' for ' '
		$path  =~ s/([^a-zA-Z0-9_.\-\/ ])/sprintf("%%%02X", ord($1))/eg;
		$owner =~ s/([^a-zA-Z0-9_.\-\/ ])/sprintf("%%%02X", ord($1))/eg;
		$path  =~ s/ /\+/g;
		$owner =~ s/ /\+/g;

		print "$path $owner\n";
	}
}

sub git_summary {
	my $descr = git_get_project_description($project) || "none";
	my %co = parse_commit("HEAD");
	my %cd = %co ? parse_date($co{'committer_epoch'}, $co{'committer_tz'}) : ();
	my $head = $co{'id'};

	my $owner = git_get_project_owner($project);

	my $refs = git_get_references();
	# These get_*_list functions return one more to allow us to see if
	# there are more ...
	my @taglist  = git_get_tags_list(16);
	my @headlist = git_get_heads_list(16);
	my @forklist;
	my $check_forks = gitweb_check_feature('forks');

	if ($check_forks) {
		@forklist = git_get_projects_list($project);
	}

	git_header_html();
	git_print_page_nav('summary','', $head);

	print "<div class=\"title\">&nbsp;</div>\n";
	print "<table class=\"projects_list\">\n" .
	      "<tr id=\"metadata_desc\"><td>description</td><td>" . esc_html($descr) . "</td></tr>\n" .
	      "<tr id=\"metadata_owner\"><td>owner</td><td>" . esc_html($owner) . "</td></tr>\n";
	if (defined $cd{'rfc2822'}) {
		print "<tr id=\"metadata_lchange\"><td>last change</td><td>$cd{'rfc2822'}</td></tr>\n";
	}

	# use per project git URL list in $projectroot/$project/cloneurl
	# or make project git URL from git base URL and project name
	my $url_tag = "URL";
	my @url_list = git_get_project_url_list($project);
	@url_list = map { "$_/$project" } @git_base_url_list unless @url_list;
	foreach my $git_url (@url_list) {
		next unless $git_url;
		print "<tr class=\"metadata_url\"><td>$url_tag</td><td>$git_url</td></tr>\n";
		$url_tag = "";
	}

	# Tag cloud
	my $show_ctags = gitweb_check_feature('ctags');
	if ($show_ctags) {
		my $ctags = git_get_project_ctags($project);
		my $cloud = git_populate_project_tagcloud($ctags);
		print "<tr id=\"metadata_ctags\"><td>Content tags:<br />";
		print "</td>\n<td>" unless %$ctags;
		print "<form action=\"$show_ctags\" method=\"post\"><input type=\"hidden\" name=\"p\" value=\"$project\" />Add: <input type=\"text\" name=\"t\" size=\"8\" /></form>";
		print "</td>\n<td>" if %$ctags;
		print git_show_project_tagcloud($cloud, 48);
		print "</td></tr>";
	}

	print "</table>\n";

	# If XSS prevention is on, we don't include README.html.
	# TODO: Allow a readme in some safe format.
	if (!$prevent_xss && -s "$projectroot/$project/README.html") {
		print "<div class=\"title\">readme</div>\n" .
		      "<div class=\"readme\">\n";
		insert_file("$projectroot/$project/README.html");
		print "\n</div>\n"; # class="readme"
	}

	# we need to request one more than 16 (0..15) to check if
	# those 16 are all
	my @commitlist = $head ? parse_commits($head, 17) : ();
	if (@commitlist) {
		git_print_header_div('shortlog');
		git_shortlog_body(\@commitlist, 0, 15, $refs,
		                  $#commitlist <=  15 ? undef :
		                  $cgi->a({-href => href(action=>"shortlog")}, "..."));
	}

	if (@taglist) {
		git_print_header_div('tags');
		git_tags_body(\@taglist, 0, 15,
		              $#taglist <=  15 ? undef :
		              $cgi->a({-href => href(action=>"tags")}, "..."));
	}

	if (@headlist) {
		git_print_header_div('heads');
		git_heads_body(\@headlist, $head, 0, 15,
		               $#headlist <= 15 ? undef :
		               $cgi->a({-href => href(action=>"heads")}, "..."));
	}

	if (@forklist) {
		git_print_header_div('forks');
		git_project_list_body(\@forklist, 'age', 0, 15,
		                      $#forklist <= 15 ? undef :
		                      $cgi->a({-href => href(action=>"forks")}, "..."),
		                      'no_header');
	}

	git_footer_html();
}

sub git_tag {
	my $head = git_get_head_hash($project);
	git_header_html();
	git_print_page_nav('','', $head,undef,$head);
	my %tag = parse_tag($hash);

	if (! %tag) {
		die_error(404, "Unknown tag object");
	}

	git_print_header_div('commit', esc_html($tag{'name'}), $hash);
	print "<div class=\"title_text\">\n" .
	      "<table class=\"object_header\">\n" .
	      "<tr>\n" .
	      "<td>object</td>\n" .
	      "<td>" . $cgi->a({-class => "list", -href => href(action=>$tag{'type'}, hash=>$tag{'object'})},
	                       $tag{'object'}) . "</td>\n" .
	      "<td class=\"link\">" . $cgi->a({-href => href(action=>$tag{'type'}, hash=>$tag{'object'})},
	                                      $tag{'type'}) . "</td>\n" .
	      "</tr>\n";
	if (defined($tag{'author'})) {
		git_print_authorship_rows(\%tag, 'author');
	}
	print "</table>\n\n" .
	      "</div>\n";
	print "<div class=\"page_body\">";
	my $comment = $tag{'comment'};
	foreach my $line (@$comment) {
		chomp $line;
		print esc_html($line, -nbsp=>1) . "<br/>\n";
	}
	print "</div>\n";
	git_footer_html();
}

sub git_blame_common {
	my $format = shift || 'porcelain';
	if ($format eq 'porcelain' && $cgi->param('js')) {
		$format = 'incremental';
		$action = 'blame_incremental'; # for page title etc
	}

	# permissions
	gitweb_check_feature('blame')
		or die_error(403, "Blame view not allowed");

	# error checking
	die_error(400, "No file name given") unless $file_name;
	$hash_base ||= git_get_head_hash($project);
	die_error(404, "Couldn't find base commit") unless $hash_base;
	my %co = parse_commit($hash_base)
		or die_error(404, "Commit not found");
	my $ftype = "blob";
	if (!defined $hash) {
		$hash = git_get_hash_by_path($hash_base, $file_name, "blob")
			or die_error(404, "Error looking up file");
	} else {
		$ftype = git_get_type($hash);
		if ($ftype !~ "blob") {
			die_error(400, "Object is not a blob");
		}
	}

	my $fd;
	if ($format eq 'incremental') {
		# get file contents (as base)
		open $fd, "-|", git_cmd(), 'cat-file', 'blob', $hash
			or die_error(500, "Open git-cat-file failed");
	} elsif ($format eq 'data') {
		# run git-blame --incremental
		open $fd, "-|", git_cmd(), "blame", "--incremental",
			$hash_base, "--", $file_name
			or die_error(500, "Open git-blame --incremental failed");
	} else {
		# run git-blame --porcelain
		open $fd, "-|", git_cmd(), "blame", '-p',
			$hash_base, '--', $file_name
			or die_error(500, "Open git-blame --porcelain failed");
	}

	# incremental blame data returns early
	if ($format eq 'data') {
		print $cgi->header(
			-type=>"text/plain", -charset => "utf-8",
			-status=> "200 OK");
		local $| = 1; # output autoflush
		print while <$fd>;
		close $fd
			or print "ERROR $!\n";

		print 'END';
		if (defined $t0 && gitweb_check_feature('timed')) {
			print ' '.
			      Time::HiRes::tv_interval($t0, [Time::HiRes::gettimeofday()]).
			      ' '.$number_of_git_cmds;
		}
		print "\n";

		return;
	}

	# page header
	git_header_html();
	my $formats_nav =
		$cgi->a({-href => href(action=>"blob", -replay=>1)},
		        "blob") .
		" | ";
	if ($format eq 'incremental') {
		$formats_nav .=
			$cgi->a({-href => href(action=>"blame", javascript=>0, -replay=>1)},
			        "blame") . " (non-incremental)";
	} else {
		$formats_nav .=
			$cgi->a({-href => href(action=>"blame_incremental", -replay=>1)},
			        "blame") . " (incremental)";
	}
	$formats_nav .=
		" | " .
		$cgi->a({-href => href(action=>"history", -replay=>1)},
		        "history") .
		" | " .
		$cgi->a({-href => href(action=>$action, file_name=>$file_name)},
		        "HEAD");
	git_print_page_nav('','', $hash_base,$co{'tree'},$hash_base, $formats_nav);
	git_print_header_div('commit', esc_html($co{'title'}), $hash_base);
	git_print_page_path($file_name, $ftype, $hash_base);

	# page body
	if ($format eq 'incremental') {
		print "<noscript>\n<div class=\"error\"><center><b>\n".
		      "This page requires JavaScript to run.\n Use ".
		      $cgi->a({-href => href(action=>'blame',javascript=>0,-replay=>1)},
		              'this page').
		      " instead.\n".
		      "</b></center></div>\n</noscript>\n";

		print qq!<div id="progress_bar" style="width: 100%; background-color: yellow"></div>\n!;
	}

	print qq!<div class="page_body">\n!;
	print qq!<div id="progress_info">... / ...</div>\n!
		if ($format eq 'incremental');
	print qq!<table id="blame_table" class="blame" width="100%">\n!.
	      #qq!<col width="5.5em" /><col width="2.5em" /><col width="*" />\n!.
	      qq!<thead>\n!.
	      qq!<tr><th>Commit</th><th>Line</th><th>Data</th></tr>\n!.
	      qq!</thead>\n!.
	      qq!<tbody>\n!;

	my @rev_color = qw(light dark);
	my $num_colors = scalar(@rev_color);
	my $current_color = 0;

	if ($format eq 'incremental') {
		my $color_class = $rev_color[$current_color];

		#contents of a file
		my $linenr = 0;
	LINE:
		while (my $line = <$fd>) {
			chomp $line;
			$linenr++;

			print qq!<tr id="l$linenr" class="$color_class">!.
			      qq!<td class="sha1"><a href=""> </a></td>!.
			      qq!<td class="linenr">!.
			      qq!<a class="linenr" href="">$linenr</a></td>!;
			print qq!<td class="pre">! . esc_html($line) . "</td>\n";
			print qq!</tr>\n!;
		}

	} else { # porcelain, i.e. ordinary blame
		my %metainfo = (); # saves information about commits

		# blame data
	LINE:
		while (my $line = <$fd>) {
			chomp $line;
			# the header: <SHA-1> <src lineno> <dst lineno> [<lines in group>]
			# no <lines in group> for subsequent lines in group of lines
			my ($full_rev, $orig_lineno, $lineno, $group_size) =
			   ($line =~ /^([0-9a-f]{40}) (\d+) (\d+)(?: (\d+))?$/);
			if (!exists $metainfo{$full_rev}) {
				$metainfo{$full_rev} = { 'nprevious' => 0 };
			}
			my $meta = $metainfo{$full_rev};
			my $data;
			while ($data = <$fd>) {
				chomp $data;
				last if ($data =~ s/^\t//); # contents of line
				if ($data =~ /^(\S+)(?: (.*))?$/) {
					$meta->{$1} = $2 unless exists $meta->{$1};
				}
				if ($data =~ /^previous /) {
					$meta->{'nprevious'}++;
				}
			}
			my $short_rev = substr($full_rev, 0, 8);
			my $author = $meta->{'author'};
			my %date =
				parse_date($meta->{'author-time'}, $meta->{'author-tz'});
			my $date = $date{'iso-tz'};
			if ($group_size) {
				$current_color = ($current_color + 1) % $num_colors;
			}
			my $tr_class = $rev_color[$current_color];
			$tr_class .= ' boundary' if (exists $meta->{'boundary'});
			$tr_class .= ' no-previous' if ($meta->{'nprevious'} == 0);
			$tr_class .= ' multiple-previous' if ($meta->{'nprevious'} > 1);
			print "<tr id=\"l$lineno\" class=\"$tr_class\">\n";
			if ($group_size) {
				print "<td class=\"sha1\"";
				print " title=\"". esc_html($author) . ", $date\"";
				print " rowspan=\"$group_size\"" if ($group_size > 1);
				print ">";
				print $cgi->a({-href => href(action=>"commit",
				                             hash=>$full_rev,
				                             file_name=>$file_name)},
				              esc_html($short_rev));
				if ($group_size >= 2) {
					my @author_initials = ($author =~ /\b([[:upper:]])\B/g);
					if (@author_initials) {
						print "<br />" .
						      esc_html(join('', @author_initials));
						#           or join('.', ...)
					}
				}
				print "</td>\n";
			}
			# 'previous' <sha1 of parent commit> <filename at commit>
			if (exists $meta->{'previous'} &&
			    $meta->{'previous'} =~ /^([a-fA-F0-9]{40}) (.*)$/) {
				$meta->{'parent'} = $1;
				$meta->{'file_parent'} = unquote($2);
			}
			my $linenr_commit =
				exists($meta->{'parent'}) ?
				$meta->{'parent'} : $full_rev;
			my $linenr_filename =
				exists($meta->{'file_parent'}) ?
				$meta->{'file_parent'} : unquote($meta->{'filename'});
			my $blamed = href(action => 'blame',
			                  file_name => $linenr_filename,
			                  hash_base => $linenr_commit);
			print "<td class=\"linenr\">";
			print $cgi->a({ -href => "$blamed#l$orig_lineno",
			                -class => "linenr" },
			              esc_html($lineno));
			print "</td>";
			print "<td class=\"pre\">" . esc_html($data) . "</td>\n";
			print "</tr>\n";
		} # end while

	}

	# footer
	print "</tbody>\n".
	      "</table>\n"; # class="blame"
	print "</div>\n";   # class="blame_body"
	close $fd
		or print "Reading blob failed\n";

	git_footer_html();
}

sub git_blame {
	git_blame_common();
}

sub git_blame_incremental {
	git_blame_common('incremental');
}

sub git_blame_data {
	git_blame_common('data');
}

sub git_tags {
	my $head = git_get_head_hash($project);
	git_header_html();
	git_print_page_nav('','', $head,undef,$head);
	git_print_header_div('summary', $project);

	my @tagslist = git_get_tags_list();
	if (@tagslist) {
		git_tags_body(\@tagslist);
	}
	git_footer_html();
}

sub git_heads {
	my $head = git_get_head_hash($project);
	git_header_html();
	git_print_page_nav('','', $head,undef,$head);
	git_print_header_div('summary', $project);

	my @headslist = git_get_heads_list();
	if (@headslist) {
		git_heads_body(\@headslist, $head);
	}
	git_footer_html();
}

sub git_blob_plain {
	my $type = shift;
	my $expires;

	if (!defined $hash) {
		if (defined $file_name) {
			my $base = $hash_base || git_get_head_hash($project);
			$hash = git_get_hash_by_path($base, $file_name, "blob")
				or die_error(404, "Cannot find file");
		} else {
			die_error(400, "No file name defined");
		}
	} elsif ($hash =~ m/^[0-9a-fA-F]{40}$/) {
		# blobs defined by non-textual hash id's can be cached
		$expires = "+1d";
	}

	open my $fd, "-|", git_cmd(), "cat-file", "blob", $hash
		or die_error(500, "Open git-cat-file blob '$hash' failed");

	# content-type (can include charset)
	$type = blob_contenttype($fd, $file_name, $type);

	# "save as" filename, even when no $file_name is given
	my $save_as = "$hash";
	if (defined $file_name) {
		$save_as = $file_name;
	} elsif ($type =~ m/^text\//) {
		$save_as .= '.txt';
	}

	# With XSS prevention on, blobs of all types except a few known safe
	# ones are served with "Content-Disposition: attachment" to make sure
	# they don't run in our security domain.  For certain image types,
	# blob view writes an <img> tag referring to blob_plain view, and we
	# want to be sure not to break that by serving the image as an
	# attachment (though Firefox 3 doesn't seem to care).
	my $sandbox = $prevent_xss &&
		$type !~ m!^(?:text/plain|image/(?:gif|png|jpeg))$!;

	print $cgi->header(
		-type => $type,
		-expires => $expires,
		-content_disposition =>
			($sandbox ? 'attachment' : 'inline')
			. '; filename="' . $save_as . '"');
	local $/ = undef;
	binmode STDOUT, ':raw';
	print <$fd>;
	binmode STDOUT, ':utf8'; # as set at the beginning of gitweb.cgi
	close $fd;
}

sub git_blob {
	my $expires;

	if (!defined $hash) {
		if (defined $file_name) {
			my $base = $hash_base || git_get_head_hash($project);
			$hash = git_get_hash_by_path($base, $file_name, "blob")
				or die_error(404, "Cannot find file");
		} else {
			die_error(400, "No file name defined");
		}
	} elsif ($hash =~ m/^[0-9a-fA-F]{40}$/) {
		# blobs defined by non-textual hash id's can be cached
		$expires = "+1d";
	}

	my $have_blame = gitweb_check_feature('blame');
	open my $fd, "-|", git_cmd(), "cat-file", "blob", $hash
		or die_error(500, "Couldn't cat $file_name, $hash");
	my $mimetype = blob_mimetype($fd, $file_name);
	# use 'blob_plain' (aka 'raw') view for files that cannot be displayed
	if ($mimetype !~ m!^(?:text/|image/(?:gif|png|jpeg)$)! && -B $fd) {
		close $fd;
		return git_blob_plain($mimetype);
	}
	# we can have blame only for text/* mimetype
	$have_blame &&= ($mimetype =~ m!^text/!);

	my $highlight = gitweb_check_feature('highlight');
	my $syntax = guess_file_syntax($highlight, $mimetype, $file_name);
	$fd = run_highlighter($fd, $highlight, $syntax)
		if $syntax;

	git_header_html(undef, $expires);
	my $formats_nav = '';
	if (defined $hash_base && (my %co = parse_commit($hash_base))) {
		if (defined $file_name) {
			if ($have_blame) {
				$formats_nav .=
					$cgi->a({-href => href(action=>"blame", -replay=>1)},
					        "blame") .
					" | ";
			}
			$formats_nav .=
				$cgi->a({-href => href(action=>"history", -replay=>1)},
				        "history") .
				" | " .
				$cgi->a({-href => href(action=>"blob_plain", -replay=>1)},
				        "raw") .
				" | " .
				$cgi->a({-href => href(action=>"blob",
				                       hash_base=>"HEAD", file_name=>$file_name)},
				        "HEAD");
		} else {
			$formats_nav .=
				$cgi->a({-href => href(action=>"blob_plain", -replay=>1)},
				        "raw");
		}
		git_print_page_nav('','', $hash_base,$co{'tree'},$hash_base, $formats_nav);
		git_print_header_div('commit', esc_html($co{'title'}), $hash_base);
	} else {
		print "<div class=\"page_nav\">\n" .
		      "<br/><br/></div>\n" .
		      "<div class=\"title\">$hash</div>\n";
	}
	git_print_page_path($file_name, "blob", $hash_base);
	print "<div class=\"page_body\">\n";
	if ($mimetype =~ m!^image/!) {
		print qq!<img type="$mimetype"!;
		if ($file_name) {
			print qq! alt="$file_name" title="$file_name"!;
		}
		print qq! src="! .
		      href(action=>"blob_plain", hash=>$hash,
		           hash_base=>$hash_base, file_name=>$file_name) .
		      qq!" />\n!;
	} else {
		my $nr;
		while (my $line = <$fd>) {
			chomp $line;
			$nr++;
			$line = untabify($line);
			printf qq!<div class="pre"><a id="l%i" href="%s#l%i" class="linenr">%4i</a> %s</div>\n!,
			       $nr, href(-replay => 1), $nr, $nr, $syntax ? $line : esc_html($line, -nbsp=>1);
		}
	}
	close $fd
		or print "Reading blob failed.\n";
	print "</div>";
	git_footer_html();
}

sub git_tree {
	if (!defined $hash_base) {
		$hash_base = "HEAD";
	}
	if (!defined $hash) {
		if (defined $file_name) {
			$hash = git_get_hash_by_path($hash_base, $file_name, "tree");
		} else {
			$hash = $hash_base;
		}
	}
	die_error(404, "No such tree") unless defined($hash);

	my $show_sizes = gitweb_check_feature('show-sizes');
	my $have_blame = gitweb_check_feature('blame');

	my @entries = ();
	{
		local $/ = "\0";
		open my $fd, "-|", git_cmd(), "ls-tree", '-z',
			($show_sizes ? '-l' : ()), @extra_options, $hash
			or die_error(500, "Open git-ls-tree failed");
		@entries = map { chomp; $_ } <$fd>;
		close $fd
			or die_error(404, "Reading tree failed");
	}

	my $refs = git_get_references();
	my $ref = format_ref_marker($refs, $hash_base);
	git_header_html();
	my $basedir = '';
	if (defined $hash_base && (my %co = parse_commit($hash_base))) {
		my @views_nav = ();
		if (defined $file_name) {
			push @views_nav,
				$cgi->a({-href => href(action=>"history", -replay=>1)},
				        "history"),
				$cgi->a({-href => href(action=>"tree",
				                       hash_base=>"HEAD", file_name=>$file_name)},
				        "HEAD"),
		}
		my $snapshot_links = format_snapshot_links($hash);
		if (defined $snapshot_links) {
			# FIXME: Should be available when we have no hash base as well.
			push @views_nav, $snapshot_links;
		}
		git_print_page_nav('tree','', $hash_base, undef, undef,
		                   join(' | ', @views_nav));
		git_print_header_div('commit', esc_html($co{'title'}) . $ref, $hash_base);
	} else {
		undef $hash_base;
		print "<div class=\"page_nav\">\n";
		print "<br/><br/></div>\n";
		print "<div class=\"title\">$hash</div>\n";
	}
	if (defined $file_name) {
		$basedir = $file_name;
		if ($basedir ne '' && substr($basedir, -1) ne '/') {
			$basedir .= '/';
		}
		git_print_page_path($file_name, 'tree', $hash_base);
	}
	print "<div class=\"page_body\">\n";
	print "<table class=\"tree\">\n";
	my $alternate = 1;
	# '..' (top directory) link if possible
	if (defined $hash_base &&
	    defined $file_name && $file_name =~ m![^/]+$!) {
		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;

		my $up = $file_name;
		$up =~ s!/?[^/]+$!!;
		undef $up unless $up;
		# based on git_print_tree_entry
		print '<td class="mode">' . mode_str('040000') . "</td>\n";
		print '<td class="size">&nbsp;</td>'."\n" if $show_sizes;
		print '<td class="list">';
		print $cgi->a({-href => href(action=>"tree",
		                             hash_base=>$hash_base,
		                             file_name=>$up)},
		              "..");
		print "</td>\n";
		print "<td class=\"link\"></td>\n";

		print "</tr>\n";
	}
	foreach my $line (@entries) {
		my %t = parse_ls_tree_line($line, -z => 1, -l => $show_sizes);

		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;

		git_print_tree_entry(\%t, $basedir, $hash_base, $have_blame);

		print "</tr>\n";
	}
	print "</table>\n" .
	      "</div>";
	git_footer_html();
}

sub snapshot_name {
	my ($project, $hash) = @_;

	# path/to/project.git  -> project
	# path/to/project/.git -> project
	my $name = to_utf8($project);
	$name =~ s,([^/])/*\.git$,$1,;
	$name = basename($name);
	# sanitize name
	$name =~ s/[[:cntrl:]]/?/g;

	my $ver = $hash;
	if ($hash =~ /^[0-9a-fA-F]+$/) {
		# shorten SHA-1 hash
		my $full_hash = git_get_full_hash($project, $hash);
		if ($full_hash =~ /^$hash/ && length($hash) > 7) {
			$ver = git_get_short_hash($project, $hash);
		}
	} elsif ($hash =~ m!^refs/tags/(.*)$!) {
		# tags don't need shortened SHA-1 hash
		$ver = $1;
	} else {
		# branches and other need shortened SHA-1 hash
		if ($hash =~ m!^refs/(?:heads|remotes)/(.*)$!) {
			$ver = $1;
		}
		$ver .= '-' . git_get_short_hash($project, $hash);
	}
	# in case of hierarchical branch names
	$ver =~ s!/!.!g;

	# name = project-version_string
	$name = "$name-$ver";

	return wantarray ? ($name, $name) : $name;
}

sub git_snapshot {
	my $format = $input_params{'snapshot_format'};
	if (!@snapshot_fmts) {
		die_error(403, "Snapshots not allowed");
	}
	# default to first supported snapshot format
	$format ||= $snapshot_fmts[0];
	if ($format !~ m/^[a-z0-9]+$/) {
		die_error(400, "Invalid snapshot format parameter");
	} elsif (!exists($known_snapshot_formats{$format})) {
		die_error(400, "Unknown snapshot format");
	} elsif ($known_snapshot_formats{$format}{'disabled'}) {
		die_error(403, "Snapshot format not allowed");
	} elsif (!grep($_ eq $format, @snapshot_fmts)) {
		die_error(403, "Unsupported snapshot format");
	}

	my $type = git_get_type("$hash^{}");
	if (!$type) {
		die_error(404, 'Object does not exist');
	}  elsif ($type eq 'blob') {
		die_error(400, 'Object is not a tree-ish');
	}

	my ($name, $prefix) = snapshot_name($project, $hash);
	my $filename = "$name$known_snapshot_formats{$format}{'suffix'}";
	my $cmd = quote_command(
		git_cmd(), 'archive',
		"--format=$known_snapshot_formats{$format}{'format'}",
		"--prefix=$prefix/", $hash);
	if (exists $known_snapshot_formats{$format}{'compressor'}) {
		$cmd .= ' | ' . quote_command(@{$known_snapshot_formats{$format}{'compressor'}});
	}

	$filename =~ s/(["\\])/\\$1/g;
	print $cgi->header(
		-type => $known_snapshot_formats{$format}{'type'},
		-content_disposition => 'inline; filename="' . $filename . '"',
		-status => '200 OK');

	open my $fd, "-|", $cmd
		or die_error(500, "Execute git-archive failed");
	binmode STDOUT, ':raw';
	print <$fd>;
	binmode STDOUT, ':utf8'; # as set at the beginning of gitweb.cgi
	close $fd;
}

sub git_log_generic {
	my ($fmt_name, $body_subr, $base, $parent, $file_name, $file_hash) = @_;

	my $head = git_get_head_hash($project);
	if (!defined $base) {
		$base = $head;
	}
	if (!defined $page) {
		$page = 0;
	}
	my $refs = git_get_references();

	my $commit_hash = $base;
	if (defined $parent) {
		$commit_hash = "$parent..$base";
	}
	my @commitlist =
		parse_commits($commit_hash, 101, (100 * $page),
		              defined $file_name ? ($file_name, "--full-history") : ());

	my $ftype;
	if (!defined $file_hash && defined $file_name) {
		# some commits could have deleted file in question,
		# and not have it in tree, but one of them has to have it
		for (my $i = 0; $i < @commitlist; $i++) {
			$file_hash = git_get_hash_by_path($commitlist[$i]{'id'}, $file_name);
			last if defined $file_hash;
		}
	}
	if (defined $file_hash) {
		$ftype = git_get_type($file_hash);
	}
	if (defined $file_name && !defined $ftype) {
		die_error(500, "Unknown type of object");
	}
	my %co;
	if (defined $file_name) {
		%co = parse_commit($base)
			or die_error(404, "Unknown commit object");
	}


	my $paging_nav = format_paging_nav($fmt_name, $page, $#commitlist >= 100);
	my $next_link = '';
	if ($#commitlist >= 100) {
		$next_link =
			$cgi->a({-href => href(-replay=>1, page=>$page+1),
			         -accesskey => "n", -title => "Alt-n"}, "next");
	}
	my $patch_max = gitweb_get_feature('patches');
	if ($patch_max && !defined $file_name) {
		if ($patch_max < 0 || @commitlist <= $patch_max) {
			$paging_nav .= " &sdot; " .
				$cgi->a({-href => href(action=>"patches", -replay=>1)},
					"patches");
		}
	}

	git_header_html();
	git_print_page_nav($fmt_name,'', $hash,$hash,$hash, $paging_nav);
	if (defined $file_name) {
		git_print_header_div('commit', esc_html($co{'title'}), $base);
	} else {
		git_print_header_div('summary', $project)
	}
	git_print_page_path($file_name, $ftype, $hash_base)
		if (defined $file_name);

	$body_subr->(\@commitlist, 0, 99, $refs, $next_link,
	             $file_name, $file_hash, $ftype);

	git_footer_html();
}

sub git_log {
	git_log_generic('log', \&git_log_body,
	                $hash, $hash_parent);
}

sub git_commit {
	$hash ||= $hash_base || "HEAD";
	my %co = parse_commit($hash)
	    or die_error(404, "Unknown commit object");

	my $parent  = $co{'parent'};
	my $parents = $co{'parents'}; # listref

	# we need to prepare $formats_nav before any parameter munging
	my $formats_nav;
	if (!defined $parent) {
		# --root commitdiff
		$formats_nav .= '(initial)';
	} elsif (@$parents == 1) {
		# single parent commit
		$formats_nav .=
			'(parent: ' .
			$cgi->a({-href => href(action=>"commit",
			                       hash=>$parent)},
			        esc_html(substr($parent, 0, 7))) .
			')';
	} else {
		# merge commit
		$formats_nav .=
			'(merge: ' .
			join(' ', map {
				$cgi->a({-href => href(action=>"commit",
				                       hash=>$_)},
				        esc_html(substr($_, 0, 7)));
			} @$parents ) .
			')';
	}
	if (gitweb_check_feature('patches') && @$parents <= 1) {
		$formats_nav .= " | " .
			$cgi->a({-href => href(action=>"patch", -replay=>1)},
				"patch");
	}

	if (!defined $parent) {
		$parent = "--root";
	}
	my @difftree;
	open my $fd, "-|", git_cmd(), "diff-tree", '-r', "--no-commit-id",
		@diff_opts,
		(@$parents <= 1 ? $parent : '-c'),
		$hash, "--"
		or die_error(500, "Open git-diff-tree failed");
	@difftree = map { chomp; $_ } <$fd>;
	close $fd or die_error(404, "Reading git-diff-tree failed");

	# non-textual hash id's can be cached
	my $expires;
	if ($hash =~ m/^[0-9a-fA-F]{40}$/) {
		$expires = "+1d";
	}
	my $refs = git_get_references();
	my $ref = format_ref_marker($refs, $co{'id'});

	git_header_html(undef, $expires);
	git_print_page_nav('commit', '',
	                   $hash, $co{'tree'}, $hash,
	                   $formats_nav);

	if (defined $co{'parent'}) {
		git_print_header_div('commitdiff', esc_html($co{'title'}) . $ref, $hash);
	} else {
		git_print_header_div('tree', esc_html($co{'title'}) . $ref, $co{'tree'}, $hash);
	}
	print "<div class=\"title_text\">\n" .
	      "<table class=\"object_header\">\n";
	git_print_authorship_rows(\%co);
	print "<tr><td>commit</td><td class=\"sha1\">$co{'id'}</td></tr>\n";
	print "<tr>" .
	      "<td>tree</td>" .
	      "<td class=\"sha1\">" .
	      $cgi->a({-href => href(action=>"tree", hash=>$co{'tree'}, hash_base=>$hash),
	               class => "list"}, $co{'tree'}) .
	      "</td>" .
	      "<td class=\"link\">" .
	      $cgi->a({-href => href(action=>"tree", hash=>$co{'tree'}, hash_base=>$hash)},
	              "tree");
	my $snapshot_links = format_snapshot_links($hash);
	if (defined $snapshot_links) {
		print " | " . $snapshot_links;
	}
	print "</td>" .
	      "</tr>\n";

	foreach my $par (@$parents) {
		print "<tr>" .
		      "<td>parent</td>" .
		      "<td class=\"sha1\">" .
		      $cgi->a({-href => href(action=>"commit", hash=>$par),
		               class => "list"}, $par) .
		      "</td>" .
		      "<td class=\"link\">" .
		      $cgi->a({-href => href(action=>"commit", hash=>$par)}, "commit") .
		      " | " .
		      $cgi->a({-href => href(action=>"commitdiff", hash=>$hash, hash_parent=>$par)}, "diff") .
		      "</td>" .
		      "</tr>\n";
	}
	print "</table>".
	      "</div>\n";

	print "<div class=\"page_body\">\n";
	git_print_log($co{'comment'});
	print "</div>\n";

	git_difftree_body(\@difftree, $hash, @$parents);

	git_footer_html();
}

sub git_object {
	# object is defined by:
	# - hash or hash_base alone
	# - hash_base and file_name
	my $type;

	# - hash or hash_base alone
	if ($hash || ($hash_base && !defined $file_name)) {
		my $object_id = $hash || $hash_base;

		open my $fd, "-|", quote_command(
			git_cmd(), 'cat-file', '-t', $object_id) . ' 2> /dev/null'
			or die_error(404, "Object does not exist");
		$type = <$fd>;
		chomp $type;
		close $fd
			or die_error(404, "Object does not exist");

	# - hash_base and file_name
	} elsif ($hash_base && defined $file_name) {
		$file_name =~ s,/+$,,;

		system(git_cmd(), "cat-file", '-e', $hash_base) == 0
			or die_error(404, "Base object does not exist");

		# here errors should not hapen
		open my $fd, "-|", git_cmd(), "ls-tree", $hash_base, "--", $file_name
			or die_error(500, "Open git-ls-tree failed");
		my $line = <$fd>;
		close $fd;

		#'100644 blob 0fa3f3a66fb6a137f6ec2c19351ed4d807070ffa	panic.c'
		unless ($line && $line =~ m/^([0-9]+) (.+) ([0-9a-fA-F]{40})\t/) {
			die_error(404, "File or directory for given base does not exist");
		}
		$type = $2;
		$hash = $3;
	} else {
		die_error(400, "Not enough information to find object");
	}

	print $cgi->redirect(-uri => href(action=>$type, -full=>1,
	                                  hash=>$hash, hash_base=>$hash_base,
	                                  file_name=>$file_name),
	                     -status => '302 Found');
}

sub git_blobdiff {
	my $format = shift || 'html';

	my $fd;
	my @difftree;
	my %diffinfo;
	my $expires;

	# preparing $fd and %diffinfo for git_patchset_body
	# new style URI
	if (defined $hash_base && defined $hash_parent_base) {
		if (defined $file_name) {
			# read raw output
			open $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
				$hash_parent_base, $hash_base,
				"--", (defined $file_parent ? $file_parent : ()), $file_name
				or die_error(500, "Open git-diff-tree failed");
			@difftree = map { chomp; $_ } <$fd>;
			close $fd
				or die_error(404, "Reading git-diff-tree failed");
			@difftree
				or die_error(404, "Blob diff not found");

		} elsif (defined $hash &&
		         $hash =~ /[0-9a-fA-F]{40}/) {
			# try to find filename from $hash

			# read filtered raw output
			open $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
				$hash_parent_base, $hash_base, "--"
				or die_error(500, "Open git-diff-tree failed");
			@difftree =
				# ':100644 100644 03b21826... 3b93d5e7... M	ls-files.c'
				# $hash == to_id
				grep { /^:[0-7]{6} [0-7]{6} [0-9a-fA-F]{40} $hash/ }
				map { chomp; $_ } <$fd>;
			close $fd
				or die_error(404, "Reading git-diff-tree failed");
			@difftree
				or die_error(404, "Blob diff not found");

		} else {
			die_error(400, "Missing one of the blob diff parameters");
		}

		if (@difftree > 1) {
			die_error(400, "Ambiguous blob diff specification");
		}

		%diffinfo = parse_difftree_raw_line($difftree[0]);
		$file_parent ||= $diffinfo{'from_file'} || $file_name;
		$file_name   ||= $diffinfo{'to_file'};

		$hash_parent ||= $diffinfo{'from_id'};
		$hash        ||= $diffinfo{'to_id'};

		# non-textual hash id's can be cached
		if ($hash_base =~ m/^[0-9a-fA-F]{40}$/ &&
		    $hash_parent_base =~ m/^[0-9a-fA-F]{40}$/) {
			$expires = '+1d';
		}

		# open patch output
		open $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
			'-p', ($format eq 'html' ? "--full-index" : ()),
			$hash_parent_base, $hash_base,
			"--", (defined $file_parent ? $file_parent : ()), $file_name
			or die_error(500, "Open git-diff-tree failed");
	}

	# old/legacy style URI -- not generated anymore since 1.4.3.
	if (!%diffinfo) {
		die_error('404 Not Found', "Missing one of the blob diff parameters")
	}

	# header
	if ($format eq 'html') {
		my $formats_nav =
			$cgi->a({-href => href(action=>"blobdiff_plain", -replay=>1)},
			        "raw");
		git_header_html(undef, $expires);
		if (defined $hash_base && (my %co = parse_commit($hash_base))) {
			git_print_page_nav('','', $hash_base,$co{'tree'},$hash_base, $formats_nav);
			git_print_header_div('commit', esc_html($co{'title'}), $hash_base);
		} else {
			print "<div class=\"page_nav\"><br/>$formats_nav<br/></div>\n";
			print "<div class=\"title\">$hash vs $hash_parent</div>\n";
		}
		if (defined $file_name) {
			git_print_page_path($file_name, "blob", $hash_base);
		} else {
			print "<div class=\"page_path\"></div>\n";
		}

	} elsif ($format eq 'plain') {
		print $cgi->header(
			-type => 'text/plain',
			-charset => 'utf-8',
			-expires => $expires,
			-content_disposition => 'inline; filename="' . "$file_name" . '.patch"');

		print "X-Git-Url: " . $cgi->self_url() . "\n\n";

	} else {
		die_error(400, "Unknown blobdiff format");
	}

	# patch
	if ($format eq 'html') {
		print "<div class=\"page_body\">\n";

		git_patchset_body($fd, [ \%diffinfo ], $hash_base, $hash_parent_base);
		close $fd;

		print "</div>\n"; # class="page_body"
		git_footer_html();

	} else {
		while (my $line = <$fd>) {
			$line =~ s!a/($hash|$hash_parent)!'a/'.esc_path($diffinfo{'from_file'})!eg;
			$line =~ s!b/($hash|$hash_parent)!'b/'.esc_path($diffinfo{'to_file'})!eg;

			print $line;

			last if $line =~ m!^\+\+\+!;
		}
		local $/ = undef;
		print <$fd>;
		close $fd;
	}
}

sub git_blobdiff_plain {
	git_blobdiff('plain');
}

sub git_commitdiff {
	my %params = @_;
	my $format = $params{-format} || 'html';

	my ($patch_max) = gitweb_get_feature('patches');
	if ($format eq 'patch') {
		die_error(403, "Patch view not allowed") unless $patch_max;
	}

	$hash ||= $hash_base || "HEAD";
	my %co = parse_commit($hash)
	    or die_error(404, "Unknown commit object");

	# choose format for commitdiff for merge
	if (! defined $hash_parent && @{$co{'parents'}} > 1) {
		$hash_parent = '--cc';
	}
	# we need to prepare $formats_nav before almost any parameter munging
	my $formats_nav;
	if ($format eq 'html') {
		$formats_nav =
			$cgi->a({-href => href(action=>"commitdiff_plain", -replay=>1)},
			        "raw");
		if ($patch_max && @{$co{'parents'}} <= 1) {
			$formats_nav .= " | " .
				$cgi->a({-href => href(action=>"patch", -replay=>1)},
					"patch");
		}

		if (defined $hash_parent &&
		    $hash_parent ne '-c' && $hash_parent ne '--cc') {
			# commitdiff with two commits given
			my $hash_parent_short = $hash_parent;
			if ($hash_parent =~ m/^[0-9a-fA-F]{40}$/) {
				$hash_parent_short = substr($hash_parent, 0, 7);
			}
			$formats_nav .=
				' (from';
			for (my $i = 0; $i < @{$co{'parents'}}; $i++) {
				if ($co{'parents'}[$i] eq $hash_parent) {
					$formats_nav .= ' parent ' . ($i+1);
					last;
				}
			}
			$formats_nav .= ': ' .
				$cgi->a({-href => href(action=>"commitdiff",
				                       hash=>$hash_parent)},
				        esc_html($hash_parent_short)) .
				')';
		} elsif (!$co{'parent'}) {
			# --root commitdiff
			$formats_nav .= ' (initial)';
		} elsif (scalar @{$co{'parents'}} == 1) {
			# single parent commit
			$formats_nav .=
				' (parent: ' .
				$cgi->a({-href => href(action=>"commitdiff",
				                       hash=>$co{'parent'})},
				        esc_html(substr($co{'parent'}, 0, 7))) .
				')';
		} else {
			# merge commit
			if ($hash_parent eq '--cc') {
				$formats_nav .= ' | ' .
					$cgi->a({-href => href(action=>"commitdiff",
					                       hash=>$hash, hash_parent=>'-c')},
					        'combined');
			} else { # $hash_parent eq '-c'
				$formats_nav .= ' | ' .
					$cgi->a({-href => href(action=>"commitdiff",
					                       hash=>$hash, hash_parent=>'--cc')},
					        'compact');
			}
			$formats_nav .=
				' (merge: ' .
				join(' ', map {
					$cgi->a({-href => href(action=>"commitdiff",
					                       hash=>$_)},
					        esc_html(substr($_, 0, 7)));
				} @{$co{'parents'}} ) .
				')';
		}
	}

	my $hash_parent_param = $hash_parent;
	if (!defined $hash_parent_param) {
		# --cc for multiple parents, --root for parentless
		$hash_parent_param =
			@{$co{'parents'}} > 1 ? '--cc' : $co{'parent'} || '--root';
	}

	# read commitdiff
	my $fd;
	my @difftree;
	if ($format eq 'html') {
		open $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
			"--no-commit-id", "--patch-with-raw", "--full-index",
			$hash_parent_param, $hash, "--"
			or die_error(500, "Open git-diff-tree failed");

		while (my $line = <$fd>) {
			chomp $line;
			# empty line ends raw part of diff-tree output
			last unless $line;
			push @difftree, scalar parse_difftree_raw_line($line);
		}

	} elsif ($format eq 'plain') {
		open $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
			'-p', $hash_parent_param, $hash, "--"
			or die_error(500, "Open git-diff-tree failed");
	} elsif ($format eq 'patch') {
		# For commit ranges, we limit the output to the number of
		# patches specified in the 'patches' feature.
		# For single commits, we limit the output to a single patch,
		# diverging from the git-format-patch default.
		my @commit_spec = ();
		if ($hash_parent) {
			if ($patch_max > 0) {
				push @commit_spec, "-$patch_max";
			}
			push @commit_spec, '-n', "$hash_parent..$hash";
		} else {
			if ($params{-single}) {
				push @commit_spec, '-1';
			} else {
				if ($patch_max > 0) {
					push @commit_spec, "-$patch_max";
				}
				push @commit_spec, "-n";
			}
			push @commit_spec, '--root', $hash;
		}
		open $fd, "-|", git_cmd(), "format-patch", @diff_opts,
			'--encoding=utf8', '--stdout', @commit_spec
			or die_error(500, "Open git-format-patch failed");
	} else {
		die_error(400, "Unknown commitdiff format");
	}

	# non-textual hash id's can be cached
	my $expires;
	if ($hash =~ m/^[0-9a-fA-F]{40}$/) {
		$expires = "+1d";
	}

	# write commit message
	if ($format eq 'html') {
		my $refs = git_get_references();
		my $ref = format_ref_marker($refs, $co{'id'});

		git_header_html(undef, $expires);
		git_print_page_nav('commitdiff','', $hash,$co{'tree'},$hash, $formats_nav);
		git_print_header_div('commit', esc_html($co{'title'}) . $ref, $hash);
		print "<div class=\"title_text\">\n" .
		      "<table class=\"object_header\">\n";
		git_print_authorship_rows(\%co);
		print "</table>".
		      "</div>\n";
		print "<div class=\"page_body\">\n";
		if (@{$co{'comment'}} > 1) {
			print "<div class=\"log\">\n";
			git_print_log($co{'comment'}, -final_empty_line=> 1, -remove_title => 1);
			print "</div>\n"; # class="log"
		}

	} elsif ($format eq 'plain') {
		my $refs = git_get_references("tags");
		my $tagname = git_get_rev_name_tags($hash);
		my $filename = basename($project) . "-$hash.patch";

		print $cgi->header(
			-type => 'text/plain',
			-charset => 'utf-8',
			-expires => $expires,
			-content_disposition => 'inline; filename="' . "$filename" . '"');
		my %ad = parse_date($co{'author_epoch'}, $co{'author_tz'});
		print "From: " . to_utf8($co{'author'}) . "\n";
		print "Date: $ad{'rfc2822'} ($ad{'tz_local'})\n";
		print "Subject: " . to_utf8($co{'title'}) . "\n";

		print "X-Git-Tag: $tagname\n" if $tagname;
		print "X-Git-Url: " . $cgi->self_url() . "\n\n";

		foreach my $line (@{$co{'comment'}}) {
			print to_utf8($line) . "\n";
		}
		print "---\n\n";
	} elsif ($format eq 'patch') {
		my $filename = basename($project) . "-$hash.patch";

		print $cgi->header(
			-type => 'text/plain',
			-charset => 'utf-8',
			-expires => $expires,
			-content_disposition => 'inline; filename="' . "$filename" . '"');
	}

	# write patch
	if ($format eq 'html') {
		my $use_parents = !defined $hash_parent ||
			$hash_parent eq '-c' || $hash_parent eq '--cc';
		git_difftree_body(\@difftree, $hash,
		                  $use_parents ? @{$co{'parents'}} : $hash_parent);
		print "<br/>\n";

		git_patchset_body($fd, \@difftree, $hash,
		                  $use_parents ? @{$co{'parents'}} : $hash_parent);
		close $fd;
		print "</div>\n"; # class="page_body"
		git_footer_html();

	} elsif ($format eq 'plain') {
		local $/ = undef;
		print <$fd>;
		close $fd
			or print "Reading git-diff-tree failed\n";
	} elsif ($format eq 'patch') {
		local $/ = undef;
		print <$fd>;
		close $fd
			or print "Reading git-format-patch failed\n";
	}
}

sub git_commitdiff_plain {
	git_commitdiff(-format => 'plain');
}

# format-patch-style patches
sub git_patch {
	git_commitdiff(-format => 'patch', -single => 1);
}

sub git_patches {
	git_commitdiff(-format => 'patch');
}

sub git_history {
	git_log_generic('history', \&git_history_body,
	                $hash_base, $hash_parent_base,
	                $file_name, $hash);
}

sub git_search {
	gitweb_check_feature('search') or die_error(403, "Search is disabled");
	if (!defined $searchtext) {
		die_error(400, "Text field is empty");
	}
	if (!defined $hash) {
		$hash = git_get_head_hash($project);
	}
	my %co = parse_commit($hash);
	if (!%co) {
		die_error(404, "Unknown commit object");
	}
	if (!defined $page) {
		$page = 0;
	}

	$searchtype ||= 'commit';
	if ($searchtype eq 'pickaxe') {
		# pickaxe may take all resources of your box and run for several minutes
		# with every query - so decide by yourself how public you make this feature
		gitweb_check_feature('pickaxe')
		    or die_error(403, "Pickaxe is disabled");
	}
	if ($searchtype eq 'grep') {
		gitweb_check_feature('grep')
		    or die_error(403, "Grep is disabled");
	}

	git_header_html();

	if ($searchtype eq 'commit' or $searchtype eq 'author' or $searchtype eq 'committer') {
		my $greptype;
		if ($searchtype eq 'commit') {
			$greptype = "--grep=";
		} elsif ($searchtype eq 'author') {
			$greptype = "--author=";
		} elsif ($searchtype eq 'committer') {
			$greptype = "--committer=";
		}
		$greptype .= $searchtext;
		my @commitlist = parse_commits($hash, 101, (100 * $page), undef,
		                               $greptype, '--regexp-ignore-case',
		                               $search_use_regexp ? '--extended-regexp' : '--fixed-strings');

		my $paging_nav = '';
		if ($page > 0) {
			$paging_nav .=
				$cgi->a({-href => href(action=>"search", hash=>$hash,
				                       searchtext=>$searchtext,
				                       searchtype=>$searchtype)},
				        "first");
			$paging_nav .= " &sdot; " .
				$cgi->a({-href => href(-replay=>1, page=>$page-1),
				         -accesskey => "p", -title => "Alt-p"}, "prev");
		} else {
			$paging_nav .= "first";
			$paging_nav .= " &sdot; prev";
		}
		my $next_link = '';
		if ($#commitlist >= 100) {
			$next_link =
				$cgi->a({-href => href(-replay=>1, page=>$page+1),
				         -accesskey => "n", -title => "Alt-n"}, "next");
			$paging_nav .= " &sdot; $next_link";
		} else {
			$paging_nav .= " &sdot; next";
		}

		if ($#commitlist >= 100) {
		}

		git_print_page_nav('','', $hash,$co{'tree'},$hash, $paging_nav);
		git_print_header_div('commit', esc_html($co{'title'}), $hash);
		git_search_grep_body(\@commitlist, 0, 99, $next_link);
	}

	if ($searchtype eq 'pickaxe') {
		git_print_page_nav('','', $hash,$co{'tree'},$hash);
		git_print_header_div('commit', esc_html($co{'title'}), $hash);

		print "<table class=\"pickaxe search\">\n";
		my $alternate = 1;
		local $/ = "\n";
		open my $fd, '-|', git_cmd(), '--no-pager', 'log', @diff_opts,
			'--pretty=format:%H', '--no-abbrev', '--raw', "-S$searchtext",
			($search_use_regexp ? '--pickaxe-regex' : ());
		undef %co;
		my @files;
		while (my $line = <$fd>) {
			chomp $line;
			next unless $line;

			my %set = parse_difftree_raw_line($line);
			if (defined $set{'commit'}) {
				# finish previous commit
				if (%co) {
					print "</td>\n" .
					      "<td class=\"link\">" .
					      $cgi->a({-href => href(action=>"commit", hash=>$co{'id'})}, "commit") .
					      " | " .
					      $cgi->a({-href => href(action=>"tree", hash=>$co{'tree'}, hash_base=>$co{'id'})}, "tree");
					print "</td>\n" .
					      "</tr>\n";
				}

				if ($alternate) {
					print "<tr class=\"dark\">\n";
				} else {
					print "<tr class=\"light\">\n";
				}
				$alternate ^= 1;
				%co = parse_commit($set{'commit'});
				my $author = chop_and_escape_str($co{'author_name'}, 15, 5);
				print "<td title=\"$co{'age_string_age'}\"><i>$co{'age_string_date'}</i></td>\n" .
				      "<td><i>$author</i></td>\n" .
				      "<td>" .
				      $cgi->a({-href => href(action=>"commit", hash=>$co{'id'}),
				              -class => "list subject"},
				              chop_and_escape_str($co{'title'}, 50) . "<br/>");
			} elsif (defined $set{'to_id'}) {
				next if ($set{'to_id'} =~ m/^0{40}$/);

				print $cgi->a({-href => href(action=>"blob", hash_base=>$co{'id'},
				                             hash=>$set{'to_id'}, file_name=>$set{'to_file'}),
				              -class => "list"},
				              "<span class=\"match\">" . esc_path($set{'file'}) . "</span>") .
				      "<br/>\n";
			}
		}
		close $fd;

		# finish last commit (warning: repetition!)
		if (%co) {
			print "</td>\n" .
			      "<td class=\"link\">" .
			      $cgi->a({-href => href(action=>"commit", hash=>$co{'id'})}, "commit") .
			      " | " .
			      $cgi->a({-href => href(action=>"tree", hash=>$co{'tree'}, hash_base=>$co{'id'})}, "tree");
			print "</td>\n" .
			      "</tr>\n";
		}

		print "</table>\n";
	}

	if ($searchtype eq 'grep') {
		git_print_page_nav('','', $hash,$co{'tree'},$hash);
		git_print_header_div('commit', esc_html($co{'title'}), $hash);

		print "<table class=\"grep_search\">\n";
		my $alternate = 1;
		my $matches = 0;
		local $/ = "\n";
		open my $fd, "-|", git_cmd(), 'grep', '-n',
			$search_use_regexp ? ('-E', '-i') : '-F',
			$searchtext, $co{'tree'};
		my $lastfile = '';
		while (my $line = <$fd>) {
			chomp $line;
			my ($file, $lno, $ltext, $binary);
			last if ($matches++ > 1000);
			if ($line =~ /^Binary file (.+) matches$/) {
				$file = $1;
				$binary = 1;
			} else {
				(undef, $file, $lno, $ltext) = split(/:/, $line, 4);
			}
			if ($file ne $lastfile) {
				$lastfile and print "</td></tr>\n";
				if ($alternate++) {
					print "<tr class=\"dark\">\n";
				} else {
					print "<tr class=\"light\">\n";
				}
				print "<td class=\"list\">".
					$cgi->a({-href => href(action=>"blob", hash=>$co{'hash'},
							       file_name=>"$file"),
						-class => "list"}, esc_path($file));
				print "</td><td>\n";
				$lastfile = $file;
			}
			if ($binary) {
				print "<div class=\"binary\">Binary file</div>\n";
			} else {
				$ltext = untabify($ltext);
				if ($ltext =~ m/^(.*)($search_regexp)(.*)$/i) {
					$ltext = esc_html($1, -nbsp=>1);
					$ltext .= '<span class="match">';
					$ltext .= esc_html($2, -nbsp=>1);
					$ltext .= '</span>';
					$ltext .= esc_html($3, -nbsp=>1);
				} else {
					$ltext = esc_html($ltext, -nbsp=>1);
				}
				print "<div class=\"pre\">" .
					$cgi->a({-href => href(action=>"blob", hash=>$co{'hash'},
							       file_name=>"$file").'#l'.$lno,
						-class => "linenr"}, sprintf('%4i', $lno))
					. ' ' .  $ltext . "</div>\n";
			}
		}
		if ($lastfile) {
			print "</td></tr>\n";
			if ($matches > 1000) {
				print "<div class=\"diff nodifferences\">Too many matches, listing trimmed</div>\n";
			}
		} else {
			print "<div class=\"diff nodifferences\">No matches found</div>\n";
		}
		close $fd;

		print "</table>\n";
	}
	git_footer_html();
}

sub git_search_help {
	git_header_html();
	git_print_page_nav('','', $hash,$hash,$hash);
	print <<EOT;
<p><strong>Pattern</strong> is by default a normal string that is matched precisely (but without
regard to case, except in the case of pickaxe). However, when you check the <em>re</em> checkbox,
the pattern entered is recognized as the POSIX extended
<a href="http://en.wikipedia.org/wiki/Regular_expression">regular expression</a> (also case
insensitive).</p>
<dl>
<dt><b>commit</b></dt>
<dd>The commit messages and authorship information will be scanned for the given pattern.</dd>
EOT
	my $have_grep = gitweb_check_feature('grep');
	if ($have_grep) {
		print <<EOT;
<dt><b>grep</b></dt>
<dd>All files in the currently selected tree (HEAD unless you are explicitly browsing
    a different one) are searched for the given pattern. On large trees, this search can take
a while and put some strain on the server, so please use it with some consideration. Note that
due to git-grep peculiarity, currently if regexp mode is turned off, the matches are
case-sensitive.</dd>
EOT
	}
	print <<EOT;
<dt><b>author</b></dt>
<dd>Name and e-mail of the change author and date of birth of the patch will be scanned for the given pattern.</dd>
<dt><b>committer</b></dt>
<dd>Name and e-mail of the committer and date of commit will be scanned for the given pattern.</dd>
EOT
	my $have_pickaxe = gitweb_check_feature('pickaxe');
	if ($have_pickaxe) {
		print <<EOT;
<dt><b>pickaxe</b></dt>
<dd>All commits that caused the string to appear or disappear from any file (changes that
added, removed or "modified" the string) will be listed. This search can take a while and
takes a lot of strain on the server, so please use it wisely. Note that since you may be
interested even in changes just changing the case as well, this search is case sensitive.</dd>
EOT
	}
	print "</dl>\n";
	git_footer_html();
}

sub git_shortlog {
	git_log_generic('shortlog', \&git_shortlog_body,
	                $hash, $hash_parent);
}

## ======================================================================
## ======================================================================
## edits

sub git_addrepo {
	if (-f $projects_list) {
		git_header_html();
		git_print_page_nav('addrepo');
		if(param('sf')) {
			open my $fd, '<', $projects_list or return;
			$fd .= "\n".escape(param('path'))." ";
		}
		git_print_header_div('summary',$project);
		print "<div class=\"page_body\">";
		print "<form action=\"$my_url\" method=\"post\"><br/>";
		print "<table><tr class=\"dark\"><td>";
		print "Repository path for \$project_list: </td><td><input style=\"width:400px;\" type=\"text\" name=\"path\"/>";
		print "</td></tr><tr class=\"light\"><td align=\"center\" colspan=\"2\">";
		print "<input type=\"submit\" value=\"Add repository\" name=\"sf\"/>";
		print "</td></tr></table></form></div>";
		git_footer_html();
	} else {
		die_error(404, "Needed a static file with list of repositories (\$project_list)");
	}
}

sub git_newrepo {
	git_header_html();
	git_print_page_nav('newrepo');
	git_print_header_div('summary',$project);

	print "<br/><div class=\"title\">Create new repository</div>";
	print "<form action=\"$my_url\" method=\"post\"><br/>";
	print "<table><tr class=\"dark\"><td>";
	print "Absolute Repository path: </td><td><input style=\"width:400px;\" type=\"text\" name=\"path\"/>";
	print "</td></tr><tr class=\"light\"><td align=\"center\" colspan=\"2\">";
	print "<input type=\"submit\" value=\"Create repository\" name=\"sf\"/>";
	print "</td></tr></table></form>";
	print "<div class=\"title\">Clone a repository</div>";
	print "<form action=\"$my_url\" method=\"post\"><br/>";
	print "<table><tr class=\"dark\"><td>";
	print "Absolute Repository path: </td><td><input style=\"width:400px;\" type=\"text\" name=\"path\"/>";
	print "</td></tr><tr class=\"light\"><td align=\"center\" colspan=\"2\">";
	print "<input type=\"submit\" value=\"Clone repository\" name=\"sf\"/>";
	print "</td></tr></table></form>";
	
	git_footer_html();
}

sub git_add {
	open my $fd, "-|", git_cmd(), "add", $file_name
		or die_error(500,"Open git-add failed");
}

sub git_rm {
	open my $fd, "-|", git_cmd(), "rm", $file_name
		or die_error(500,"Open git-rm failed");
}

sub git_reset {
	open my $fd, "-|", git_cmd(), "reset", "HEAD", $file_name
		or die_error(500,"Open git-reset failed");
}

sub git_discard {
	git_reset();
	open my $fd, "-|", git_cmd(), "checkout", "--", $file_name
		or die_error(500,"Open git-checkout failed");
}

sub git_mv {
	git_header_html();
	git_print_page_nav();
	git_print_header_div('status',$project);
	if (param('sf')) {
		open my $fh, "-|", git_cmd(), "mv", param('src'), param('dest')
			or die_error(500, "Open git-mv failed");
	}
	print "<div class=\"page_body\">";
	print "<form action=\"$my_url\" method=\"post\"><br/>";
	print "<table><tr class=\"dark\"><td>";
	print "Source file name: </td><td><input style=\"width:400px;\" type=\"text\" name=\"src\"/>";
	print "</td></tr><tr class=\"light\"><td>";
	print "Destination file path: </td><td><input style=\"width:400px;\" type=\"text\" name=\"dest\"/>";
	print "</td></tr><tr class=\"dark\"><td align=\"center\" colspan=\"2\">";
	print "<input type=\"submit\" value=\"Move file\" name=\"sf\"/>";
	print "</td></tr></table></form></div>";
	git_footer_html();
}

sub git_ignore {
	# Ex: "/home/pavan/Coding" / "gsoc/code/.git" "ignore"
	my $gitignore = "$projectroot/$project"."ignore";
	my $fd = '';
	if (-f $gitignore) {
		open $fd, '<', $gitignore or return;
	}
	$fd .= "\n$file_name";
	open $fd, '>', $gitignore or return;
}

sub git_commit_code {
	open my $fd, "-|", git_cmd(), "commit", "-s", "-m",
		"\"".param('subject').' '.param('body')."\""
		or die_error(500,"Open git-commit failed");
	git_header_html();
	git_print_page_nav();
	git_print_header_div('status',$project);
	while (my $line = <$fd>) {
		chomp $line;
		print $line."<br/>";
	}
	git_footer_html();
}

sub git_tag_create {
	open my $fd, "-|", git_cmd(), "tag", "-s", "-m",
		"\"".param('msg')."\""
		or die_error(500,"Open git-tag failed");
}

sub git_branch_create {
	open my $fd, "-|", git_cmd(), "branch", param('name')
		or die_error(500,"Open git-branch failed");
}

sub git_branch_delete_without_merge {
	open my $fd, "-|", git_cmd(), "branch", "-D", param('name')
		or die_error(500,"Open git-branch failed");
}

sub git_branch_delete_with_merge {
	open my $fd, "-|", git_cmd(), "branch", "-d", param('name')
		or die_error(500,"Open git-branch failed");
}

sub git_branch_move {
	open my $fd, "-|", git_cmd(), "branch", "-m",
		param('oldname'), param('newname')
		or die_error(500,"Open git-branch failed");
}

sub git_merge {
	open my $fd, "-|", git_cmd(), "merge"
		param('first_branch'), param('second_branch')
		or die_error(500,"Open git-merge failed");
}

#	git_header_html();
#	git_print_page_nav();
#	git_print_header_div('status',$project);
#	git_footer_html();

## ......................................................................
## feeds (RSS, Atom; OPML)

sub git_feed {
	my $format = shift || 'atom';
	my $have_blame = gitweb_check_feature('blame');

	# Atom: http://www.atomenabled.org/developers/syndication/
	# RSS:  http://www.notestips.com/80256B3A007F2692/1/NAMO5P9UPQ
	if ($format ne 'rss' && $format ne 'atom') {
		die_error(400, "Unknown web feed format");
	}

	# log/feed of current (HEAD) branch, log of given branch, history of file/directory
	my $head = $hash || 'HEAD';
	my @commitlist = parse_commits($head, 150, 0, $file_name);

	my %latest_commit;
	my %latest_date;
	my $content_type = "application/$format+xml";
	if (defined $cgi->http('HTTP_ACCEPT') &&
		 $cgi->Accept('text/xml') > $cgi->Accept($content_type)) {
		# browser (feed reader) prefers text/xml
		$content_type = 'text/xml';
	}
	if (defined($commitlist[0])) {
		%latest_commit = %{$commitlist[0]};
		my $latest_epoch = $latest_commit{'committer_epoch'};
		%latest_date   = parse_date($latest_epoch);
		my $if_modified = $cgi->http('IF_MODIFIED_SINCE');
		if (defined $if_modified) {
			my $since;
			if (eval { require HTTP::Date; 1; }) {
				$since = HTTP::Date::str2time($if_modified);
			} elsif (eval { require Time::ParseDate; 1; }) {
				$since = Time::ParseDate::parsedate($if_modified, GMT => 1);
			}
			if (defined $since && $latest_epoch <= $since) {
				print $cgi->header(
					-type => $content_type,
					-charset => 'utf-8',
					-last_modified => $latest_date{'rfc2822'},
					-status => '304 Not Modified');
				return;
			}
		}
		print $cgi->header(
			-type => $content_type,
			-charset => 'utf-8',
			-last_modified => $latest_date{'rfc2822'});
	} else {
		print $cgi->header(
			-type => $content_type,
			-charset => 'utf-8');
	}

	# Optimization: skip generating the body if client asks only
	# for Last-Modified date.
	return if ($cgi->request_method() eq 'HEAD');

	# header variables
	my $title = "$site_name - $project/$action";
	my $feed_type = 'log';
	if (defined $hash) {
		$title .= " - '$hash'";
		$feed_type = 'branch log';
		if (defined $file_name) {
			$title .= " :: $file_name";
			$feed_type = 'history';
		}
	} elsif (defined $file_name) {
		$title .= " - $file_name";
		$feed_type = 'history';
	}
	$title .= " $feed_type";
	my $descr = git_get_project_description($project);
	if (defined $descr) {
		$descr = esc_html($descr);
	} else {
		$descr = "$project " .
		         ($format eq 'rss' ? 'RSS' : 'Atom') .
		         " feed";
	}
	my $owner = git_get_project_owner($project);
	$owner = esc_html($owner);

	#header
	my $alt_url;
	if (defined $file_name) {
		$alt_url = href(-full=>1, action=>"history", hash=>$hash, file_name=>$file_name);
	} elsif (defined $hash) {
		$alt_url = href(-full=>1, action=>"log", hash=>$hash);
	} else {
		$alt_url = href(-full=>1, action=>"summary");
	}
	print qq!<?xml version="1.0" encoding="utf-8"?>\n!;
	if ($format eq 'rss') {
		print <<XML;
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
<channel>
XML
		print "<title>$title</title>\n" .
		      "<link>$alt_url</link>\n" .
		      "<description>$descr</description>\n" .
		      "<language>en</language>\n" .
		      # project owner is responsible for 'editorial' content
		      "<managingEditor>$owner</managingEditor>\n";
		if (defined $logo || defined $favicon) {
			# prefer the logo to the favicon, since RSS
			# doesn't allow both
			my $img = esc_url($logo || $favicon);
			print "<image>\n" .
			      "<url>$img</url>\n" .
			      "<title>$title</title>\n" .
			      "<link>$alt_url</link>\n" .
			      "</image>\n";
		}
		if (%latest_date) {
			print "<pubDate>$latest_date{'rfc2822'}</pubDate>\n";
			print "<lastBuildDate>$latest_date{'rfc2822'}</lastBuildDate>\n";
		}
		print "<generator>gitweb v.$version/$git_version</generator>\n";
	} elsif ($format eq 'atom') {
		print <<XML;
<feed xmlns="http://www.w3.org/2005/Atom">
XML
		print "<title>$title</title>\n" .
		      "<subtitle>$descr</subtitle>\n" .
		      '<link rel="alternate" type="text/html" href="' .
		      $alt_url . '" />' . "\n" .
		      '<link rel="self" type="' . $content_type . '" href="' .
		      $cgi->self_url() . '" />' . "\n" .
		      "<id>" . href(-full=>1) . "</id>\n" .
		      # use project owner for feed author
		      "<author><name>$owner</name></author>\n";
		if (defined $favicon) {
			print "<icon>" . esc_url($favicon) . "</icon>\n";
		}
		if (defined $logo_url) {
			# not twice as wide as tall: 72 x 27 pixels
			print "<logo>" . esc_url($logo) . "</logo>\n";
		}
		if (! %latest_date) {
			# dummy date to keep the feed valid until commits trickle in:
			print "<updated>1970-01-01T00:00:00Z</updated>\n";
		} else {
			print "<updated>$latest_date{'iso-8601'}</updated>\n";
		}
		print "<generator version='$version/$git_version'>gitweb</generator>\n";
	}

	# contents
	for (my $i = 0; $i <= $#commitlist; $i++) {
		my %co = %{$commitlist[$i]};
		my $commit = $co{'id'};
		# we read 150, we always show 30 and the ones more recent than 48 hours
		if (($i >= 20) && ((time - $co{'author_epoch'}) > 48*60*60)) {
			last;
		}
		my %cd = parse_date($co{'author_epoch'});

		# get list of changed files
		open my $fd, "-|", git_cmd(), "diff-tree", '-r', @diff_opts,
			$co{'parent'} || "--root",
			$co{'id'}, "--", (defined $file_name ? $file_name : ())
			or next;
		my @difftree = map { chomp; $_ } <$fd>;
		close $fd
			or next;

		# print element (entry, item)
		my $co_url = href(-full=>1, action=>"commitdiff", hash=>$commit);
		if ($format eq 'rss') {
			print "<item>\n" .
			      "<title>" . esc_html($co{'title'}) . "</title>\n" .
			      "<author>" . esc_html($co{'author'}) . "</author>\n" .
			      "<pubDate>$cd{'rfc2822'}</pubDate>\n" .
			      "<guid isPermaLink=\"true\">$co_url</guid>\n" .
			      "<link>$co_url</link>\n" .
			      "<description>" . esc_html($co{'title'}) . "</description>\n" .
			      "<content:encoded>" .
			      "<![CDATA[\n";
		} elsif ($format eq 'atom') {
			print "<entry>\n" .
			      "<title type=\"html\">" . esc_html($co{'title'}) . "</title>\n" .
			      "<updated>$cd{'iso-8601'}</updated>\n" .
			      "<author>\n" .
			      "  <name>" . esc_html($co{'author_name'}) . "</name>\n";
			if ($co{'author_email'}) {
				print "  <email>" . esc_html($co{'author_email'}) . "</email>\n";
			}
			print "</author>\n" .
			      # use committer for contributor
			      "<contributor>\n" .
			      "  <name>" . esc_html($co{'committer_name'}) . "</name>\n";
			if ($co{'committer_email'}) {
				print "  <email>" . esc_html($co{'committer_email'}) . "</email>\n";
			}
			print "</contributor>\n" .
			      "<published>$cd{'iso-8601'}</published>\n" .
			      "<link rel=\"alternate\" type=\"text/html\" href=\"$co_url\" />\n" .
			      "<id>$co_url</id>\n" .
			      "<content type=\"xhtml\" xml:base=\"" . esc_url($my_url) . "\">\n" .
			      "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n";
		}
		my $comment = $co{'comment'};
		print "<pre>\n";
		foreach my $line (@$comment) {
			$line = esc_html($line);
			print "$line\n";
		}
		print "</pre><ul>\n";
		foreach my $difftree_line (@difftree) {
			my %difftree = parse_difftree_raw_line($difftree_line);
			next if !$difftree{'from_id'};

			my $file = $difftree{'file'} || $difftree{'to_file'};

			print "<li>" .
			      "[" .
			      $cgi->a({-href => href(-full=>1, action=>"blobdiff",
			                             hash=>$difftree{'to_id'}, hash_parent=>$difftree{'from_id'},
			                             hash_base=>$co{'id'}, hash_parent_base=>$co{'parent'},
			                             file_name=>$file, file_parent=>$difftree{'from_file'}),
			              -title => "diff"}, 'D');
			if ($have_blame) {
				print $cgi->a({-href => href(-full=>1, action=>"blame",
				                             file_name=>$file, hash_base=>$commit),
				              -title => "blame"}, 'B');
			}
			# if this is not a feed of a file history
			if (!defined $file_name || $file_name ne $file) {
				print $cgi->a({-href => href(-full=>1, action=>"history",
				                             file_name=>$file, hash=>$commit),
				              -title => "history"}, 'H');
			}
			$file = esc_path($file);
			print "] ".
			      "$file</li>\n";
		}
		if ($format eq 'rss') {
			print "</ul>]]>\n" .
			      "</content:encoded>\n" .
			      "</item>\n";
		} elsif ($format eq 'atom') {
			print "</ul>\n</div>\n" .
			      "</content>\n" .
			      "</entry>\n";
		}
	}

	# end of feed
	if ($format eq 'rss') {
		print "</channel>\n</rss>\n";
	} elsif ($format eq 'atom') {
		print "</feed>\n";
	}
}

sub git_rss {
	git_feed('rss');
}

sub git_atom {
	git_feed('atom');
}

sub git_opml {
	my @list = git_get_projects_list();

	print $cgi->header(
		-type => 'text/xml',
		-charset => 'utf-8',
		-content_disposition => 'inline; filename="opml.xml"');

	print <<XML;
<?xml version="1.0" encoding="utf-8"?>
<opml version="1.0">
<head>
  <title>$site_name OPML Export</title>
</head>
<body>
<outline text="git RSS feeds">
XML

	foreach my $pr (@list) {
		my %proj = %$pr;
		my $head = git_get_head_hash($proj{'path'});
		if (!defined $head) {
			next;
		}
		$git_dir = "$projectroot/$proj{'path'}";
		my %co = parse_commit($head);
		if (!%co) {
			next;
		}

		my $path = esc_html(chop_str($proj{'path'}, 25, 5));
		my $rss  = href('project' => $proj{'path'}, 'action' => 'rss', -full => 1);
		my $html = href('project' => $proj{'path'}, 'action' => 'summary', -full => 1);
		print "<outline type=\"rss\" text=\"$path\" title=\"$path\" xmlUrl=\"$rss\" htmlUrl=\"$html\"/>\n";
	}
	print <<XML;
</outline>
</body>
</opml>
XML
}
