#!/usr/bin/perl
#
# Gitweb::Git -- gitweb's package dealing with running git commands
#
# This program is licensed under the GPLv2

package Gitweb::Git;

use strict;
use warnings;
use Exporter qw(import);

our @EXPORT = qw($GIT $number_of_git_cmds $git_version $git_dir
                 git_cmd quote_command evaluate_git_version);

# core git executable to use
# this can just be "git" if your webserver has a sensible PATH
our $GIT;

our $number_of_git_cmds = 0;

# version of the core git binary
our $git_version;

# path to the current git repository
our $git_dir;

# returns path to the core git executable and the --git-dir parameter as list
sub git_cmd {
	$number_of_git_cmds++;
	return $GIT, '--git-dir='.$git_dir;
}

# quote the given arguments for passing them to the shell
# quote_command("command", "arg 1", "arg with ' and ! characters")
# => "'command' 'arg 1' 'arg with '\'' and '\!' characters'"
# Try to avoid using this function wherever possible.
sub quote_command {
	return join(' ',
		map { my $a = $_; $a =~ s/(['!])/'\\$1'/g; "'$a'" } @_ );
}

sub evaluate_git_version {
	$git_version = qx("$GIT" --version) =~ m/git version (.*)$/ ? $1 : "unknown";
	$number_of_git_cmds++;
}

1;
