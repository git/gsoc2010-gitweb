#!/usr/bin/perl
#
# Gitweb::Config -- gitweb configuration package
#
# This program is licensed under the GPLv2

package Gitweb::Config;

use strict;
use warnings;
use Exporter qw(import);

our @EXPORT = qw(evaluate_gitweb_config gitweb_check_feature gitweb_get_feature configure_gitweb_features
                 filter_snapshot_fmts %known_snapshot_formats $version $projectroot $project_maxdepth
                 $projects_list @git_base_url_list $export_ok $strict_export $home_link_str $site_name
                 $site_header $site_footer $home_text @stylesheets $stylesheet $logo $favicon $javascript
                 $GITWEB_CONFIG $GITWEB_CONFIG_SYSTEM $logo_url $logo_label $export_auth_hook
                 $projects_list_description_width $default_projects_order $default_blob_plain_mimetype
                 $default_text_plain_charset $fallback_encoding @diff_opts $prevent_xss $maxload
                 $git_avatar %avatar_size %feature @snapshot_fmts $mimetypes_file $git_write);

use Gitweb::Git qw($git_dir);

# The following variables are affected by build-time configuration
# and hence their initialisation is put in gitweb.perl script

our $version;

# absolute fs-path which will be prepended to the project path
#our $projectroot = "/pub/scm";
our $projectroot;
# fs traversing limit for getting project list
# the number is relative to the projectroot
our $project_maxdepth;
# source of projects list
our $projects_list;
# list of git base URLs used for URL to where fetch project from,
# i.e. full URL is "$git_base_url/$project"
our @git_base_url_list;

# show repository only if this file exists
# (only effective if this variable evaluates to true)
our $export_ok;
# only allow viewing of repositories also shown on the overview page
our $strict_export;

# string of the home link on top of all pages
our $home_link_str;
# name of your site or organization to appear in page titles
# replace this with something more descriptive for clearer bookmarks
our $site_name;
# filename of html text to include at top of each page
our $site_header;
# filename of html text to include at bottom of each page
our $site_footer;
# html text to include at home page
our $home_text;

# URI of stylesheets
our @stylesheets;
# URI of a single stylesheet, which can be overridden in GITWEB_CONFIG.
our $stylesheet;
# URI of GIT logo (72x27 size)
our $logo;
# URI of GIT favicon, assumed to be image/png type
our $favicon;
# URI of gitweb.js (JavaScript code for gitweb)
our $javascript;

# gitweb config
our ($GITWEB_CONFIG, $GITWEB_CONFIG_SYSTEM);

# URI and label (title) of GIT logo link
#our $logo_url = "http://www.kernel.org/pub/software/scm/git/docs/";
#our $logo_label = "git documentation";
our $logo_url = "http://git-scm.com/";
our $logo_label = "git homepage";

# the width (in characters) of the projects list "Description" column
our $projects_list_description_width = 25;

# default order of projects list
# valid values are none, project, descr, owner, and age
our $default_projects_order = "project";

# show repository only if this subroutine returns true
# when given the path to the project, for example:
#    sub { return -e "$_[0]/git-daemon-export-ok"; }
our $export_auth_hook = undef;

# default blob_plain mimetype and default charset for text/plain blob
our $default_blob_plain_mimetype = 'text/plain';
our $default_text_plain_charset  = undef;

# file to use for guessing MIME types before trying /etc/mime.types
# (relative to the current git repository)
our $mimetypes_file = undef;

# assume this charset if line contains non-UTF-8 characters;
# it should be valid encoding (see Encoding::Supported(3pm) for list),
# for which encoding all byte sequences are valid, for example
# 'iso-8859-1' aka 'latin1' (it is decoded without checking, so it
# could be even 'utf-8' for the old behavior)
our $fallback_encoding = 'latin1';

# rename detection options for git-diff and git-diff-tree
# - default is '-M', with the cost proportional to
#   (number of removed files) * (number of new files).
# - more costly is '-C' (which implies '-M'), with the cost proportional to
#   (number of changed files + number of removed files) * (number of new files)
# - even more costly is '-C', '--find-copies-harder' with cost
#   (number of files in the original tree) * (number of new files)
# - one might want to include '-B' option, e.g. '-B', '-M'
our @diff_opts = ('-M'); # taken from git_commit

# Disables features that would allow repository owners to inject script into
# the gitweb domain.
our $prevent_xss = 0;

# information about snapshot formats that gitweb is capable of serving
our %known_snapshot_formats = (
	# name => {
	# 	'display' => display name,
	# 	'type' => mime type,
	# 	'suffix' => filename suffix,
	# 	'format' => --format for git-archive,
	# 	'compressor' => [compressor command and arguments]
	# 	                (array reference, optional)
	# 	'disabled' => boolean (optional)}
	#
	'tgz' => {
		'display' => 'tar.gz',
		'type' => 'application/x-gzip',
		'suffix' => '.tar.gz',
		'format' => 'tar',
		'compressor' => ['gzip']},

	'tbz2' => {
		'display' => 'tar.bz2',
		'type' => 'application/x-bzip2',
		'suffix' => '.tar.bz2',
		'format' => 'tar',
		'compressor' => ['bzip2']},

	'txz' => {
		'display' => 'tar.xz',
		'type' => 'application/x-xz',
		'suffix' => '.tar.xz',
		'format' => 'tar',
		'compressor' => ['xz'],
		'disabled' => 1},

	'zip' => {
		'display' => 'zip',
		'type' => 'application/x-zip',
		'suffix' => '.zip',
		'format' => 'zip'},
);

# Aliases so we understand old gitweb.snapshot values in repository
# configuration.
our %known_snapshot_format_aliases = (
	'gzip'  => 'tgz',
	'bzip2' => 'tbz2',
	'xz'    => 'txz',

	# backward compatibility: legacy gitweb config support
	'x-gzip' => undef, 'gz' => undef,
	'x-bzip2' => undef, 'bz2' => undef,
	'x-zip' => undef, '' => undef,
);

# Pixel sizes for icons and avatars. If the default font sizes or lineheights
# are changed, it may be appropriate to change these values too via
# $GITWEB_CONFIG.
our %avatar_size = (
	'default' => 16,
	'double'  => 32
);

# Used to set the maximum load that we will still respond to gitweb queries.
# If server load exceed this value then return "503 server busy" error.
# If gitweb cannot determined server load, it is taken to be 0.
# Leave it undefined (or set to 'undef') to turn off load checking.
our $maxload = 300;

# You define site-wide feature defaults here; override them with
# $GITWEB_CONFIG as necessary.
our %feature = (
	# feature => {
	# 	'sub' => feature-sub (subroutine),
	# 	'override' => allow-override (boolean),
	# 	'default' => [ default options...] (array reference)}
	#
	# if feature is overridable (it means that allow-override has true value),
	# then feature-sub will be called with default options as parameters;
	# return value of feature-sub indicates if to enable specified feature
	#
	# if there is no 'sub' key (no feature-sub), then feature cannot be
	# overriden
	#
	# use gitweb_get_feature(<feature>) to retrieve the <feature> value
	# (an array) or gitweb_check_feature(<feature>) to check if <feature>
	# is enabled

	# Enable the 'write' functionalities for gitweb. While outputting HTML
	# it checks for gitweb_check_feature('write') and if it's enabled,
	# proper links will appear along with the HTML divs.
	
	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'write'}{'default'} = [1];
	# Project specific override is not supported.
	'write' => {
		'override' => 0,
		'default' => [0]},

	# Enable the 'blame' blob view, showing the last commit that modified
	# each line in the file. This can be very CPU-intensive.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'blame'}{'default'} = [1];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'blame'}{'override'} = 1;
	# and in project config gitweb.blame = 0|1;
	'blame' => {
		'sub' => sub { feature_bool('blame', @_) },
		'override' => 0,
		'default' => [0]},

	# Enable the 'snapshot' link, providing a compressed archive of any
	# tree. This can potentially generate high traffic if you have large
	# project.

	# Value is a list of formats defined in %known_snapshot_formats that
	# you wish to offer.
	# To disable system wide have in $GITWEB_CONFIG
	# $feature{'snapshot'}{'default'} = [];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'snapshot'}{'override'} = 1;
	# and in project config, a comma-separated list of formats or "none"
	# to disable.  Example: gitweb.snapshot = tbz2,zip;
	'snapshot' => {
		'sub' => \&feature_snapshot,
		'override' => 0,
		'default' => ['tgz']},

	# Enable text search, which will list the commits which match author,
	# committer or commit text to a given string.  Enabled by default.
	# Project specific override is not supported.
	'search' => {
		'override' => 0,
		'default' => [1]},

	# Enable grep search, which will list the files in currently selected
	# tree containing the given string. Enabled by default. This can be
	# potentially CPU-intensive, of course.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'grep'}{'default'} = [1];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'grep'}{'override'} = 1;
	# and in project config gitweb.grep = 0|1;
	'grep' => {
		'sub' => sub { feature_bool('grep', @_) },
		'override' => 0,
		'default' => [1]},

	# Enable the pickaxe search, which will list the commits that modified
	# a given string in a file. This can be practical and quite faster
	# alternative to 'blame', but still potentially CPU-intensive.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'pickaxe'}{'default'} = [1];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'pickaxe'}{'override'} = 1;
	# and in project config gitweb.pickaxe = 0|1;
	'pickaxe' => {
		'sub' => sub { feature_bool('pickaxe', @_) },
		'override' => 0,
		'default' => [1]},

	# Enable showing size of blobs in a 'tree' view, in a separate
	# column, similar to what 'ls -l' does.  This cost a bit of IO.

	# To disable system wide have in $GITWEB_CONFIG
	# $feature{'show-sizes'}{'default'} = [0];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'show-sizes'}{'override'} = 1;
	# and in project config gitweb.showsizes = 0|1;
	'show-sizes' => {
		'sub' => sub { feature_bool('showsizes', @_) },
		'override' => 0,
		'default' => [1]},

	# Make gitweb use an alternative format of the URLs which can be
	# more readable and natural-looking: project name is embedded
	# directly in the path and the query string contains other
	# auxiliary information. All gitweb installations recognize
	# URL in either format; this configures in which formats gitweb
	# generates links.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'pathinfo'}{'default'} = [1];
	# Project specific override is not supported.

	# Note that you will need to change the default location of CSS,
	# favicon, logo and possibly other files to an absolute URL. Also,
	# if gitweb.cgi serves as your indexfile, you will need to force
	# $my_uri to contain the script name in your $GITWEB_CONFIG.
	'pathinfo' => {
		'override' => 0,
		'default' => [0]},

	# Make gitweb consider projects in project root subdirectories
	# to be forks of existing projects. Given project $projname.git,
	# projects matching $projname/*.git will not be shown in the main
	# projects list, instead a '+' mark will be added to $projname
	# there and a 'forks' view will be enabled for the project, listing
	# all the forks. If project list is taken from a file, forks have
	# to be listed after the main project.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'forks'}{'default'} = [1];
	# Project specific override is not supported.
	'forks' => {
		'override' => 0,
		'default' => [0]},

	# Insert custom links to the action bar of all project pages.
	# This enables you mainly to link to third-party scripts integrating
	# into gitweb; e.g. git-browser for graphical history representation
	# or custom web-based repository administration interface.

	# The 'default' value consists of a list of triplets in the form
	# (label, link, position) where position is the label after which
	# to insert the link and link is a format string where %n expands
	# to the project name, %f to the project path within the filesystem,
	# %h to the current hash (h gitweb parameter) and %b to the current
	# hash base (hb gitweb parameter); %% expands to %.

	# To enable system wide have in $GITWEB_CONFIG e.g.
	# $feature{'actions'}{'default'} = [('graphiclog',
	# 	'/git-browser/by-commit.html?r=%n', 'summary')];
	# Project specific override is not supported.
	'actions' => {
		'override' => 0,
		'default' => []},

	# Allow gitweb scan project content tags described in ctags/
	# of project repository, and display the popular Web 2.0-ish
	# "tag cloud" near the project list. Note that this is something
	# COMPLETELY different from the normal Git tags.

	# gitweb by itself can show existing tags, but it does not handle
	# tagging itself; you need an external application for that.
	# For an example script, check Girocco's cgi/tagproj.cgi.
	# You may want to install the HTML::TagCloud Perl module to get
	# a pretty tag cloud instead of just a list of tags.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'ctags'}{'default'} = ['path_to_tag_script'];
	# Project specific override is not supported.
	'ctags' => {
		'override' => 0,
		'default' => [0]},

	# The maximum number of patches in a patchset generated in patch
	# view. Set this to 0 or undef to disable patch view, or to a
	# negative number to remove any limit.

	# To disable system wide have in $GITWEB_CONFIG
	# $feature{'patches'}{'default'} = [0];
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'patches'}{'override'} = 1;
	# and in project config gitweb.patches = 0|n;
	# where n is the maximum number of patches allowed in a patchset.
	'patches' => {
		'sub' => \&feature_patches,
		'override' => 0,
		'default' => [16]},

	# Avatar support. When this feature is enabled, views such as
	# shortlog or commit will display an avatar associated with
	# the email of the committer(s) and/or author(s).

	# Currently available providers are gravatar and picon.
	# If an unknown provider is specified, the feature is disabled.

	# Gravatar depends on Digest::MD5.
	# Picon currently relies on the indiana.edu database.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'avatar'}{'default'} = ['<provider>'];
	# where <provider> is either gravatar or picon.
	# To have project specific config enable override in $GITWEB_CONFIG
	# $feature{'avatar'}{'override'} = 1;
	# and in project config gitweb.avatar = <provider>;
	'avatar' => {
		'sub' => \&feature_avatar,
		'override' => 0,
		'default' => ['']},

	# Enable displaying how much time and how many git commands
	# it took to generate and display page.  Disabled by default.
	# Project specific override is not supported.
	'timed' => {
		'override' => 0,
		'default' => [0]},

	# Enable turning some links into links to actions which require
	# JavaScript to run (like 'blame_incremental').  Not enabled by
	# default.  Project specific override is currently not supported.
	'javascript-actions' => {
		'override' => 0,
		'default' => [0]},

	# Syntax highlighting support. This is based on Daniel Svensson's
	# and Sham Chukoury's work in gitweb-xmms2.git.
	# It requires the 'highlight' program present in $PATH,
	# and therefore is disabled by default.

	# To enable system wide have in $GITWEB_CONFIG
	# $feature{'highlight'}{'default'} = [1];

	'highlight' => {
		'sub' => sub { feature_bool('highlight', @_) },
		'override' => 0,
		'default' => [0]},
);

sub evaluate_gitweb_config {
	# die if there are errors parsing config file
	if (-e $GITWEB_CONFIG) {
		do $GITWEB_CONFIG;
		die $@ if $@;
	} elsif (-e $GITWEB_CONFIG_SYSTEM) {
		do $GITWEB_CONFIG_SYSTEM;
		die $@ if $@;
	}
}


sub gitweb_get_feature {
	my ($name) = @_;
	return unless exists $feature{$name};
	my ($sub, $override, @defaults) = (
		$feature{$name}{'sub'},
		$feature{$name}{'override'},
		@{$feature{$name}{'default'}});
	# project specific override is possible only if we have project
	if (!$override || !defined $git_dir) {
		return @defaults;
	}
	if (!defined $sub) {
		warn "feature $name is not overridable";
		return @defaults;
	}
	return $sub->(@defaults);
}

# A wrapper to check if a given feature is enabled.
# With this, you can say
#
#   my $bool_feat = gitweb_check_feature('bool_feat');
#   gitweb_check_feature('bool_feat') or somecode;
#
# instead of
#
#   my ($bool_feat) = gitweb_get_feature('bool_feat');
#   (gitweb_get_feature('bool_feat'))[0] or somecode;
#
sub gitweb_check_feature {
	return (gitweb_get_feature(@_))[0];
}

# process alternate names for backward compatibility
# filter out unsupported (unknown) snapshot formats
sub filter_snapshot_fmts {
	my @fmts = @_;

	@fmts = map {
		exists $known_snapshot_format_aliases{$_} ?
		       $known_snapshot_format_aliases{$_} : $_} @fmts;
	@fmts = grep {
		exists $known_snapshot_formats{$_} &&
		!$known_snapshot_formats{$_}{'disabled'}} @fmts;
}

our (@snapshot_fmts, $git_avatar, $git_write);
sub configure_gitweb_features {
	# list of supported snapshot formats
	our @snapshot_fmts = gitweb_get_feature('snapshot');
	@snapshot_fmts = filter_snapshot_fmts(@snapshot_fmts);

	# A variable flag to check 'write' feature
	our $git_write = gitweb_check_feature('write');

	# check that the avatar feature is set to a known provider name,
	# and for each provider check if the dependencies are satisfied.
	# if the provider name is invalid or the dependencies are not met,
	# reset $git_avatar to the empty string.
	our ($git_avatar) = gitweb_get_feature('avatar');
	if ($git_avatar eq 'gravatar') {
		$git_avatar = '' unless (eval { require Digest::MD5; 1; });
	} elsif ($git_avatar eq 'picon') {
		# no dependencies
	} else {
		$git_avatar = '';
	}
}

1;
