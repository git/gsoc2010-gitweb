#!/usr/bin/perl
#
# Gitweb::Request -- gitweb request(cgi) package
#
# This program is licensed under the GPLv2

package Gitweb::Request;

use strict;
use warnings;
use Exporter qw(import);

our @EXPORT = qw($cgi $my_url $my_uri $base_url $path_info $home_link $action $project $file_name
                 $file_parent $hash $hash_parent $hash_base $hash_parent_base @extra_options $page $edit
                 $searchtype $search_use_regexp $searchtext $search_regexp %input_params %allowed_options
                 @cgi_param_mapping %cgi_param_mapping $t0 evaluate_query_params evaluate_uri);

our $t0;
if (eval { require Time::HiRes; 1; }) {
	$t0 = [Time::HiRes::gettimeofday()];
}

our ($cgi, $my_url, $my_uri, $base_url, $path_info, $home_link);
our ($action, $edit, $project, $file_name, $file_parent, $hash, $hash_parent, $hash_base,
     $hash_parent_base, @extra_options, $page);
our ($searchtype, $search_use_regexp, $searchtext, $search_regexp);

# input parameters can be collected from a variety of sources (presently, CGI
# and PATH_INFO), so we define an %input_params hash that collects them all
# together during validation: this allows subsequent uses (e.g. href()) to be
# agnostic of the parameter origin

our %input_params = ();

# input parameters are stored with the long parameter name as key. This will
# also be used in the href subroutine to convert parameters to their CGI
# equivalent, and since the href() usage is the most frequent one, we store
# the name -> CGI key mapping here, instead of the reverse.
#
# XXX: Warning: If you touch this, check the search form for updating,
# too.

our @cgi_param_mapping = (
	project => "p",
	action => "a",
	edit => "e",
	file_name => "f",
	file_parent => "fp",
	hash => "h",
	hash_parent => "hp",
	hash_base => "hb",
	hash_parent_base => "hpb",
	page => "pg",
	order => "o",
	searchtext => "s",
	searchtype => "st",
	snapshot_format => "sf",
	extra_options => "opt",
	search_use_regexp => "sr",
	# this must be last entry (for manipulation from JavaScript)
	javascript => "js"
);
our %cgi_param_mapping = @cgi_param_mapping;

# finally, we have the hash of allowed extra_options for the commands that
# allow them
our %allowed_options = (
	"--no-merges" => [ qw(rss atom log shortlog history) ],
);

# fill %input_params with the CGI parameters. All values except for 'opt'
# should be single values, but opt can be an array. We should probably
# build an array of parameters that can be multi-valued, but since for the time
# being it's only this one, we just single it out
sub evaluate_query_params {
	while (my ($name, $symbol) = each %cgi_param_mapping) {
		if ($symbol eq 'opt') {
			$input_params{$name} = [ $cgi->param($symbol) ];
		} else {
			$input_params{$name} = $cgi->param($symbol);
		}
	}
}

sub evaluate_uri {
	our $cgi;

	our $my_url = $cgi->url();
	our $my_uri = $cgi->url(-absolute => 1);

	# Base URL for relative URLs in gitweb ($Gitweb::Config::logo, $Gitweb::Config::favicon, ...),
	# needed and used only for URLs with nonempty PATH_INFO
	our $base_url = $my_url;

	# When the script is used as DirectoryIndex, the URL does not contain the name
	# of the script file itself, and $cgi->url() fails to strip PATH_INFO, so we
	# have to do it ourselves. We make $path_info global because it's also used
	# later on.
	#
	# Another issue with the script being the DirectoryIndex is that the resulting
	# $my_url data is not the full script URL: this is good, because we want
	# generated links to keep implying the script name if it wasn't explicitly
	# indicated in the URL we're handling, but it means that $my_url cannot be used
	# as base URL.
	# Therefore, if we needed to strip PATH_INFO, then we know that we have
	# to build the base URL ourselves:
	our $path_info = $ENV{"PATH_INFO"};
	if ($path_info) {
		if ($my_url =~ s,\Q$path_info\E$,, &&
		    $my_uri =~ s,\Q$path_info\E$,, &&
		    defined $ENV{'SCRIPT_NAME'}) {
			$base_url = $cgi->url(-base => 1) . $ENV{'SCRIPT_NAME'};
		}
	}

	# target of the home link on top of all pages
	our $home_link = $my_uri || "/";
}

1;
